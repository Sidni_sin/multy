<?php

namespace App\Models\Partners;

use App\Api\Epn\clEPNAPIAccess;
use clEPNCabinetAccess;
use Illuminate\Database\Eloquent\Model;

class Epn extends Model implements iPartner
{

    protected $_epnApiAccess = null;
    protected $_api = null;
    protected $userHash = null;

    public function __construct($userApiKey, $userPrivateKey, $userHash)
    {
        $this->userHash = $userHash;
        $this->_epnApiAccess = new clEPNAPIAccess($userApiKey, $userHash);
        parent::__construct();
    }

    public function getProduct(int $id)
    {

    }

    public function getCategoryList()
    {
        return $this->categoriesList;
    }

    public function search($query, array $params = null)
    {
        $params = array_merge(['query' => $query], $params);

        $this->_epnApiAccess->AddRequestSearch(
            'random_goods_1',
            $params
        );

        $this->_epnApiAccess->RunRequests();

        return $this->_epnApiAccess->GetRequestResult('random_goods_1')['offers'];
    }

    public function getPartnerLink($link, int $id)
    {
        $l = '?to=http://buyeasy.by/redirect/cpa/o/'.$this->getUserHash().'/&sub1='.$id;
        return $link.$l;
    }

    public function checkConnection(): bool
    {
        $this->_epnApiAccess->AddRequestGetTopMonthly('top_sales_1', 'ru', 'RUR', 'sales');
        $this->_epnApiAccess->RunRequests();

        $offers = $this->_epnApiAccess->GetRequestResult('top_sales_1')['offers'];

        return !empty($offers);
    }

    public function getUserHash()
    {
        return $this->userHash;
    }
}
