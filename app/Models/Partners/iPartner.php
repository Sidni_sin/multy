<?php

namespace App\Models\Partners;

interface iPartner
{
    public function __construct($userApiKey, $userPrivateKey, $userHash);

    public function getProduct(int $id);

    public function search($value, array $params = null);

    public function getPartnerLink($link, int $id);

    public function checkConnection() : bool;
}