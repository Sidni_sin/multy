<?php

namespace App\Models\Partners;


class Factory
{
    public function create($namePartner, $userApiKey, $privateKey, $userHash)
    {
        $namePartner = 'App\Models\Partners\\'.ucfirst(mb_strtolower($namePartner));
        return new $namePartner($userApiKey, $privateKey, $userHash);
    }
}