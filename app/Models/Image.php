<?php

namespace App\Models;

class Image extends BaseModel
{
    protected $fillable = ['src', 'alt', 'title', 'product_id'];
}
