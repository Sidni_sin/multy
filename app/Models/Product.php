<?php

namespace App\Models;

use App\Traits\Image as ImageTrait;

class Product extends BaseModel
{
    use ImageTrait;

    protected $fillable = [
        'slug', 'p_id', 'p_category_id', 'p_store_id', 'p_store_title', 'name', 'picture', 'price', 'sale_price', 'currency', 'description', 'short_description', 'specifications', 'url', 'lang', 'commission_rate'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getFolder()
    {
        return 'products';
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryProduct()
    {
        return $this->hasMany(CategoryProduct::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function newEloquentBuilder($query)
    {
        return new QueryFilters($query);
    }

    public function scopeFilter(QueryFilters $q)
    {
        $q->filterWhere('price_from', 'price', '>');
        $q->filterWhere('price_to', 'price', '<');
        $q->filterOrderByIn('price_by', 'price');
        $q->filterOrderByIn('sale', 'sale_price');
        $q->filterSearch('s', 'name', 'description', 'short_description');

        return $q;
    }

    public function breadCrumbHtml($cssClassesUl = '', $separatorStatus = true)
    {
        $c = $this->categories->first();

        $html = '';
        $html .= '<li><a href="'.route('frontend.main').'">'.'Главная'.'</a></li>';
        $html .= $separatorStatus?'<li>/</li>':'';

        if(!is_null($c)){

            $html .= '<li><a href="'.route('frontend.categories.show', $c->slug).'">'.$c->name.'</a></li>';
            $html .= $separatorStatus?'<li>/</li>':'';
        }

        $html .= '<li><a href="'.route('frontend.products.show', $this->slug).'">'.$this->name.'</a></li>';

        $resHtml = '<ul class="'.$cssClassesUl.'">' .$html. '</ul>';
        return $resHtml;
    }
}
