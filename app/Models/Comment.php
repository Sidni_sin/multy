<?php

namespace App\Models;

use App\User;

class Comment extends BaseModel
{
    CONST IS_VISIBLE = 1;
    CONST IS_NOT_VISIBLE = 0;

    protected $fillable = [
        'title', 'text', 'assessment', 'product_id', 'is_visible'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
