<?php

namespace App\Models;

class Page extends BaseModel
{
    CONST VISIBLE     = 1;
    CONST NOT_VISIBLE = 0;

    use \App\Traits\Image;

    protected $fillable = [
        'store_id', 'slug', 'image', 'title', 'description', 'text', 'keywords', 'is_visible',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function getFolder()
    {
        return 'pages';
    }
}
