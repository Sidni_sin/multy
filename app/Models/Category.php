<?php

namespace App\Models;

class Category extends BaseModel
{
    CONST PARENT_CATEGORY = 0;

    protected $fillable = [
        'slug', 'parent_id', 'name', 'description', 'keywords', 'position', 'is_visible'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function productsCount()
    {
        return $this->hasOne(CategoryProduct::class)
            ->selectRaw('category_id, count(*) as count')
            ->groupBy('category_id');
    }


    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function preparationForTree($date)
    {

        $tree = [];

        foreach ($date as $item) {
            $tree[$item['parent_id']][] = $item;
        }

        return $tree;
    }

    public function createTreeHyphen($date, $parent_id = 0, $hyphen = '', &$res = [], &$level = 1)
    {

        if(empty($date[$parent_id])){
            return;
        }

        for($i = 0; $i < count($date[$parent_id]); $i++) {

            $tmp['id']   = $date[$parent_id][$i]['id'];
            $tmp['slug'] = $date[$parent_id][$i]['slug'];
            $tmp['name'] = $hyphen.$date[$parent_id][$i]['name'];
            $tmp['description'] = $date[$parent_id][$i]['description'];
            $tmp['keywords'] = $date[$parent_id][$i]['keywords'];
            $tmp['position'] = $date[$parent_id][$i]['position'];
            $tmp['is_visible'] = $date[$parent_id][$i]['is_visible'];
            $tmp['level']= $level;

            $res[]       = $tmp;

            $hyphen .= '-';
            $level++;
            $this->createTreeHyphen($date, $date[$parent_id][$i]['id'], $hyphen, $res, $level);
            $level--;
            $hyphen = substr($hyphen, 1);

        }

        return $res;

    }

    public function createTreeHtml($date, $parent_id = 0, &$res = '')
    {
        if(empty($date[$parent_id])){
            return;
        }

        $res .= '<ul>';

        for($i = 0; $i < count($date[$parent_id]); $i++) {

            $res .= '<li>'.$date[$parent_id][$i]['name'];

            $res .= '<a href="'.route("edit_categories_a", $date[$parent_id][$i]['id']).'"> Edit </a>';
            $res .= '<a href="'.route("delete_cat_a", $date[$parent_id][$i]['id']).'">Delete </a>';

            $res .= '</li>';

            $this->createTreeHtml($date, $date[$parent_id][$i]['id'], $res);

        }
        $res .= '</ul>';

        return $res;
    }

}
