<?php

namespace App\Models;

class ExclusiveProduct extends BaseModel
{
    CONST MAIN_PRODUCT  = 1;
    CONST SUB_PRODUCT   = 0;

    protected $fillable = [
        'product_id', 'main', 'background_color', 'text_layout'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
