<?php

namespace App\Models;

class EmbedCode extends BaseModel
{
    CONST NOT_EMBED  = 0;
    CONST TOP_EMBED  = 1;
    CONST DOWN_EMBED = 2;

    protected $fillable = [
        'store_id', 'code', 'location'
    ];

    public function getTypeEmbeds()
    {
        return [
            self::NOT_EMBED,
            self::TOP_EMBED,
            self::DOWN_EMBED,
        ];
    }
}
