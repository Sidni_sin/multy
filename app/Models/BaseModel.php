<?php

namespace App\Models;

use App\Facades\SubDomainService;
use App\Observers\StoreIdObserver;
use App\Observers\StoreIdObserver as Observer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $dispatchesEvents = [
        'creating'  => StoreIdObserver::class,
        'updating'  => StoreIdObserver::class,
        'deleting'  => StoreIdObserver::class,
    ];

    protected static function boot()
    {
        parent::boot();

        static::observe(Observer::class);

        static::addGlobalScope('store_id', function (Builder $builder) {
            $builder->whereStoreId(SubDomainService::getStoreId());
        });
    }

    public function insertAll(array $items)
    {
        $now = \Carbon\Carbon::now();
        $items = collect($items)->map(function (array $data) use ($now) {
            $data = array_merge(['store_id' => SubDomainService::getStoreId()], $data);

            return $this->timestamps ? array_merge([
                'created_at' => $now,
                'updated_at' => $now,
            ], $data) : $data;
        })->all();

        return \DB::table($this->getTable())->insert($items);
    }
}
