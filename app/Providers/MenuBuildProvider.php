<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuBuildProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('menuBuildService', 'App\Services\MenuBuild');
    }
}
