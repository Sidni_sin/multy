<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EmbedCodeProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('embedCodeService', 'App\Services\EmbedCode');
    }
}
