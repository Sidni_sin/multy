<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OptionsProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('optionsService', 'App\Services\Options');
    }
}
