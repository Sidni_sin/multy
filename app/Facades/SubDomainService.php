<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SubDomainService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'subDomainService';
    }
}