<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OptionsService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'optionsService';
    }
}