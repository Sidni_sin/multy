<?php
namespace App\Scopes;

use App\Facades\SubDomainService;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class StoreScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('store_id', '=', SubDomainService::getStoreId());
    }
}