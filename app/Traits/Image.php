<?php

namespace App\Traits;

use App\Facades\SubDomainService;
use Illuminate\Support\Facades\File;
use InvalidArgumentException;
use Image as ImageF;

trait Image
{
    protected $width    = 1920;
    protected $height   = null;

    /*
     * return name folder for save images
     * */
    abstract public function getFolder();

    public function saveImage($image): string
    {
        $input['image_name'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath     = public_path(DIRECTORY_SEPARATOR.$this->getFolder().DIRECTORY_SEPARATOR.SubDomainService::getSlug().DIRECTORY_SEPARATOR);
        $i=0;
        while (file_exists($destinationPath.$input['image_name'])){
            $i++;
            $input['image_name'] = time()."_{$i}".'.'.$image->getClientOriginalExtension();
        }

        File::exists($destinationPath) or File::makeDirectory($destinationPath);

        $img = ImageF::make($image->getRealPath());
        $img->resize($this->getWidth(), $this->getHeight(), function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.$input['image_name']);

        return '/'.$this->getFolder().'/'.SubDomainService::getSlug().'/'.$input['image_name'];
    }

    public function saveImages($images)
    {
        $res = [];
        foreach ($images as $image)
            $res[] = $this->saveImage($image);

        return $res;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWidth()
    {
        return $this->width;
    }
}