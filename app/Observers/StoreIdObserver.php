<?php

namespace App\Observers;

use App\Facades\SubDomainService;

class StoreIdObserver
{
    public function creating($model)
    {
        $model->store_id = SubDomainService::getStoreId();
    }

    public function updating($model)
    {
        $model->store_id = SubDomainService::getStoreId();
    }

    public function deleting($model)
    {
        $model->store_id = SubDomainService::getStoreId();
    }
}
