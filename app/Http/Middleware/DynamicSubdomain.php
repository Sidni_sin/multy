<?php

namespace App\Http\Middleware;

use Closure;

class DynamicSubdomain
{
    public function handle($request, Closure $next)
    {
        \URL::defaults([
            'store' => $request->route()->parameter('store'),
        ]);

        foreach ($request->route()->parameters() as $p){
            if (is_object($p)){
                $request->route()->forgetParameter('store');
                break;
            }
        }

        return $next($request);
    }
}
