<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class FileBrowser
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $destinationPath = public_path(DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.Auth::id());
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            Config::set('elfinder.dir.0', Config::get('elfinder.dir.0').'/'.Auth::id());

        }
        return $next($request);
    }
}
