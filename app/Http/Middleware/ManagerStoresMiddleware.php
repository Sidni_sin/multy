<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Request;

class ManagerStoresMiddleware
{
    CONST NUMBER_PREFIX_STORE = 2;

    public function handle($request, Closure $next)
    {
        $prefix_store = explode('/', $request->getRequestUri())[self::NUMBER_PREFIX_STORE];
        $stores       = Auth::user()->stores;

        $slug = $stores->first(function ($store) use ($prefix_store) {
            return ($store->slug == $prefix_store);
        });

        if(is_null($slug)){
            return redirect()->route('dashboard.stores.index');
        }

        return $next($request);
    }
}
