<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProduct extends FormRequest
{
    public function rules()
    {
        return [
            'url'  => 'required|max:255',
            'p_id' => 'required|max:255',
            'lang' => 'required|max:255',
            'name' => 'required|max:255',
            'price' => 'required|max:255',
            'picture' => 'required|max:255',
            'currency' => 'required|max:255',
            'sale_price' => 'required|max:255',
            'description' => 'max:65000',
            'p_category_id' => 'required|max:255',
            'categories.*'          => [
                'sometimes',
                'nullable',
                Rule::exists('categories', 'id')->where(function ($query) {
                    return $query->where('store_id', SubDomainService::getStoreId());
                })
            ],
        ];
    }
}
