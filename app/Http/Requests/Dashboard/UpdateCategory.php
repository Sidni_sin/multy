<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateCategory extends FormRequest
{
    public function rules(Request $request)
    {
        $slug = $request->segment(4);

        return [
//            'slug'          => [
//                'required',
//                'between:3,255',
//                'regex:/(^([a-zA-Z]+)?$)/u',
//                Rule::unique('categories')->where(function ($query) use ($slug) {
//                    return $query->where('store_id', SubDomainService::getStoreId())->where('slug', '!=', $slug);
//                })
//            ],
            'name'          => 'required|between:3,255',
            'description'   => 'required|between:3,255',
            'keywords'      => 'required|between:3,255',
            'position'      => 'sometimes|required|integer',
            'is_visible'    => 'required|in:1,0',
        ];
    }
}
