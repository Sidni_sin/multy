<?php

namespace App\Http\Requests\Dashboard;

use App\Models\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        $segment = $request->segment(3);
        $req_slug = $request->get('slug');

        $id = ($segment == $req_slug ? ','.Store::whereSlug($req_slug)->first()->id : '');

        return [
            'name' => 'required|min:2|max:255',
            'slug' => "required|min:2|max:255|regex:/(^([a-zA-Z]+)?$)/u|unique:stores,slug{$id}",
            'user_hash'     => 'required|min:2|max:255|alpha_num',
            'user_api_key'  => 'required|min:2|max:255|alpha_num',
            'user_private_key'  => 'required|min:2|max:255|alpha_num',
        ];
    }
}
