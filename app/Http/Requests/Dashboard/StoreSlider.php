<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class StoreSlider extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'   => 'required|between:3,255',
            'text'    => 'required|between:3,255',
            'price'   => 'sometimes|nullable|numeric|min:1',
            'url_button' => 'required|url|max:255',
            'text_type'  => 'required|in:center,left,right',
            'image'   => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'background_image'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ];
    }
}
