<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $slug = $request->segment(4);

        return [
            'slug'          => [
                'required',
                'between:3,255',
                'regex:/(^([a-zA-Z]+)?$)/u',
                Rule::unique('pages')->where(function ($query) use ($slug){
                    return $query->where('store_id', SubDomainService::getStoreId())->where('slug', '!=', $slug);
                })
            ],
            'title'   => 'required|between:3,255',
            'text'    => 'required|between:3,8000',
            'image'   => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'keywords'   => 'required|between:3,255',
            'is_visible' => 'required|in:1,0',
            'description' => 'required|between:3,255',
        ];
    }
}
