<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOption extends FormRequest
{
    public function rules()
    {
        return [
            'meta_main_image' => '',
            'meta_main_title' => '',
            'meta_main_description' => '',
            'meta_main_keywords' => '',
            'meta_main_locale' => '',
            'meta_main_site' => '',
            'lang_login' => '',
            'lang_register' => '',
            'lang_main' => '',
            'lang_category' => '',
            'lang_page' => '',
            'social_page_google' => '',
            'social_page_instagram' => '',
            'social_page_twitter' => '',
            'social_page_facebook' => '',
            'social_page_vk' => '',
            'footer_columns_1_title' => '',
            'footer_columns_1_text' => '',
            'footer_columns_2_title' => '',
            'footer_columns_2_text' => '',
            'button_show_all' => '',
        ];
    }
}
