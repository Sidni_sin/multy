<?php

namespace App\Http\Requests\Dashboard;

use App\Models\EmbedCode;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmbedCode extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(EmbedCode $embedCode)
    {
        $embedType = implode(',', $embedCode->getTypeEmbeds());

        return [
            'code' => 'required|max:4000',
            'location' => 'required|in:'.$embedType,
        ];
    }
}
