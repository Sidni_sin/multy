<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCategory extends FormRequest
{
    public function rules()
    {
        return [
//            'slug'          => [
//                'required',
//                'between:3,255',
//                'regex:/(^([a-zA-Z]+)?$)/u',
//                Rule::unique('categories')->where(function ($query) {
//                    return $query->where('store_id', SubDomainService::getStoreId());
//                })
//            ],
            'parent_id'          => [
                'sometimes',
                'required',
                'integer',
                'exists:categories,id'
            ],
            'name'          => 'required|between:3,255',
            'description'   => 'required|between:3,255',
            'keywords'      => 'required|between:3,255',
            'position'      => 'sometimes|required|integer',
            'is_visible'    => 'required|in:1,0',
        ];
    }
}
