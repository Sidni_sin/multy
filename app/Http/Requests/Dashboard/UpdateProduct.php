<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateProduct extends FormRequest
{
    public function rules(Request $request)
    {
        $segment = $request->segment(4);
        $req_slug = $request->get('slug');

        $id = ($segment == $req_slug ? Product::whereSlug($req_slug)->first()->id : '');

        return [
            'lang' => 'required|max:255',
            'slug' => [
                Rule::unique('products')->where(function ($query) use ($id) {
                    return $query->where('store_id', SubDomainService::getStoreId())->where('id', '!=', $id);
                }),
            ],
            'name' => 'required|max:255',
            'price' => 'required|max:255',
            'picture' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'currency' => 'required|max:255',
            'pictures.*' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'sale_price' => 'required|max:255',
            'description' => 'max:65000',
            'short_description' => 'required|max:510',
            'specifications' => 'required|max:510|JSON',
            'categories'          => [
                'sometimes',
                'required',
                Rule::exists('categories', 'id')->where(function ($query) {
                    return $query->where('store_id', SubDomainService::getStoreId());
                })
            ],
        ];
    }
}
