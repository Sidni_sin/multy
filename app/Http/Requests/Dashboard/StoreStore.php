<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class StoreStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'slug' => 'required|min:2|max:255|regex:/(^([a-zA-Z]+)?$)/u|unique:stores',
            'user_hash'     => 'required|min:2|max:255|alpha_num',
            'user_api_key'  => 'required|min:2|max:255|alpha_num',
            'user_private_key'  => 'required|min:2|max:255|alpha_num',
        ];
    }
}
