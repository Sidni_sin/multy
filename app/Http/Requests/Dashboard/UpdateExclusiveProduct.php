<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateExclusiveProduct extends FormRequest
{
    public function rules()
    {
        return [
            'product_id' => [
                'required',
                Rule::exists('products', 'id')->where(function ($query) {
                    return $query->where('store_id', SubDomainService::getStoreId());
                })
            ],
            'main'       => 'sometimes|required|accepted',
            'text_layout' => 'sometimes|nullable|required|max:255',
            'background_color' => 'sometimes|nullable|required|max:255',
        ];
    }
}
