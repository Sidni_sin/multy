<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateSlider extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        return [
            'title'   => 'required|between:3,255',
            'text'    => 'required|between:3,255',
            'price'   => 'sometimes|nullable|numeric|min:1',
            'url_button' => 'required|url|max:255',
            'text_type'  => 'required|in:center,left,right',
            'position'  => [
                'required',
                Rule::unique('sliders')->where(function ($query) use ($request) {
                    return $query->where('store_id', SubDomainService::getStoreId())->where('position', '!=' ,$request->input('position'));
                })
            ],
            'image'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'background_image'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ];
    }
}
