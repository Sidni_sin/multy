<?php

namespace App\Http\Requests\Dashboard;

use App\Facades\SubDomainService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePage extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'slug' => [
                'required',
                'between:3,255',
                'regex:/(^([a-zA-Z]+)?$)/u',
                Rule::unique('pages')->where(function ($query) {
                    return $query->where('store_id', SubDomainService::getStoreId());
                })
            ],
            'title'   => 'required|between:3,255',
            'text'    => 'required|between:3,8000',
            'image'   => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'keywords'   => 'required|between:3,255',
            'is_visible' => 'required|in:1,0',
            'description' => 'required|between:3,255',
        ];
    }
}
