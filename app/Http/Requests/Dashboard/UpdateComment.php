<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class UpdateComment extends FormRequest
{
    public function rules()
    {
        return [
            'title'  =>  [
                'required',
                'string',
                'min:3',
                'max:255',
            ],
            'text'  =>  [
                'required',
                'string',
                'min:3',
                'max:4000',
            ],
            'assessment'  =>  [
                'required',
                'between:1,5',
            ],
            'is_visible'  =>  [
                'required',
                'in:1,0'
            ],
        ];
    }
}
