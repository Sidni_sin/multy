<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreComment extends FormRequest
{
    public function rules()
    {
        return [
            'title'  =>  [
                'required',
                'string',
                'min:3',
                'max:255',
            ],
            'text'  =>  [
                'required',
                'string',
                'min:3',
                'max:4000',
            ],
            'assessment'  =>  [
                'required',
                'between:1,5',
            ],
            'product_id'  =>  [
                'required',
                'exists:products,id'
            ],
        ];
    }
}
