<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComment;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends FrontendController
{
    public function store(StoreComment $request, Comment $comment)
    {
        $comment->fill($request->validated());
        $comment->user_id = Auth::id();
        $comment->save();
        $comment->load('user');
        return response()->json($comment);
    }

}
