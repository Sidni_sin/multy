<?php

namespace App\Http\Controllers\Dashboard;

use App\Facades\SubDomainService;
use App\Models\Partners\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerProductController extends Controller
{

    protected $partner = null;

    public function __construct(Factory $factory)
    {
        $this->partner = $factory->create(SubDomainService::getProgram(), SubDomainService::getUserApiKey(), SubDomainService::getUserPrivateKey(), SubDomainService::getUserHash());
    }

    public function search(Request $request)
    {
        $params = [
            'limit' => $request->api_limit,
            'lang' => $request->api_lang,
            'orderby' => $request->api_orderby ?? '',
            'offset' => 0,
            'category' => $asd = implode(",", $request->api_categoriesList),
            'currency' => $request->api_currency,
        ];

        return response()->json($this->partner->search($request->search, $params), 200);
    }

}
