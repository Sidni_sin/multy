<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StoreStore;
use App\Http\Requests\Dashboard\UpdateStore;
use App\Manager;
use App\Models\Admin\ConfirmationRegisterStore;
use App\Models\Option;
use App\Models\Partners\DataAdminApi;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    public function index()
    {
        return view('dashboard.stores.index', ['stores' => Auth::user()->stores, 'user' => Auth::user()]);
    }

    public function create(DataAdminApi $dataAdminApi)
    {
        $nextId = Store::latest()->first()->id + 1;
        return view('dashboard.stores.create', compact('dataAdminApi', 'nextId'));
    }

    public function store(StoreStore $request, Store $store, Option $option, DataAdminApi $adminApi)
    {
        $manager = Auth::user();
        $manager->quantity_store += 1;

        if($manager->quantity_store > $manager->limit_store){
            return redirect(route('dashboard.stores.create'))
                ->withErrors(['limit' => 'Вы достигли лимита. Если вам нужно больше магазинов, обратитесь в службу поддержки'])
                ->withInput();
        }

        $store->fill($request->validated());
        $store->manager_id = Auth::id();

        $confirmationStore = new ConfirmationRegisterStore($store);

        if(!$confirmationStore->areOurPartner()){
            return redirect(route('dashboard.stores.create'))
                ->withErrors(
                    [
                        'error' => 'Вы не являетесь нашим партнёром, в программе EPN. Зарегистрируйтесь пожалуйста через нашу <a href="'.$adminApi->getEpn()['link'].'">партнёрскую ссылку</a>'
                    ]
                )
                ->withInput();
        }

        DB::transaction(function () use ($store, $manager, $option) {
            $store->save();
            $manager->save();
            $option->seed($store->id);
        });

        return redirect()->route('dashboard.store.main', ['store' => $store->slug]);
    }

    public function show($id)
    {
        //
    }

    public function edit(Store $store)
    {
        if($store->manager_id != Auth::id()){
            return redirect()->route('dashboard.stores.index');
        }

        return view('dashboard.stores.edit', compact('store'));
    }

    public function update(UpdateStore $request, Store $store)
    {
        if($store->manager_id != Auth::id()){
            return redirect()->route('dashboard.stores.index');
        }

        $store->fill($request->validated());
        $store->save();

        return redirect()->route('dashboard.stores.index');
    }

    public function destroy($id)
    {
        //
    }
}
