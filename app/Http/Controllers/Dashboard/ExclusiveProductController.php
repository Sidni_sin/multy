<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StoreExclusiveProduct;
use App\Http\Requests\Dashboard\UpdateExclusiveProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExclusiveProduct as Exclusive;

class ExclusiveProductController extends Controller
{
    public function index()
    {
        $exclusives = Exclusive::with('product')->get();
        return view('dashboard.exclusive-products.index', compact('exclusives'));
    }

    public function create()
    {
        $products = Product::All();
        return view('dashboard.exclusive-products.create', compact('products'));
    }

    public function store(StoreExclusiveProduct $request, Exclusive $exclusive)
    {
        $exclusive->fill($request->validated());

        if(!empty($request->validated()['main'])){
            $exclusive->main = Exclusive::MAIN_PRODUCT;
            Exclusive::where('main', Exclusive::MAIN_PRODUCT)->update(['main' => Exclusive::SUB_PRODUCT]);
        }

        $exclusive->save();

        return redirect()->route('dashboard.store.exclusive-products.index');
    }

    public function edit(Exclusive $exclusiveProduct)
    {
        $products = Product::all();
        return view('dashboard.exclusive-products.edit', compact('products', 'exclusiveProduct'));
    }

    public function update(UpdateExclusiveProduct $request, Exclusive $exclusiveProduct)
    {
        $exclusiveProduct->fill($request->validated());

        if(isset($request->validated()['main'])){
            Exclusive::where('main', Exclusive::MAIN_PRODUCT)->where('id', '!=', $exclusiveProduct->id)->update(['main' => Exclusive::SUB_PRODUCT]);
            $exclusiveProduct->main = Exclusive::MAIN_PRODUCT;
        }else{
            $exclusiveProduct->main = Exclusive::SUB_PRODUCT;
        }

        $exclusiveProduct->save();

        return redirect()->route('dashboard.store.exclusive-products.index');
    }

    public function destroy(Exclusive $exclusiveProduct)
    {
        $exclusiveProduct->delete();
        return redirect()->back();
    }
}
