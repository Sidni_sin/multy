<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StorePage;
use App\Http\Requests\Dashboard\UpdatePage;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('dashboard.pages.index', compact('pages'));
    }

    public function create()
    {
        return view('dashboard.pages.create');
    }

    public function store(StorePage $request, Page $page)
    {
        $image = $request->file('image');

        $page->fill($request->validated());
        $page->image = $page->saveImage($image);

        $page->save();
        return redirect()->route('dashboard.store.pages.index');
    }

    public function show($id)
    {
        //
    }

    public function edit(Page $page)
    {
        return view('dashboard.pages.edit', compact('page'));
    }

    public function update(UpdatePage $request, Page $page)
    {
        $page->fill($request->validated());

        if(!is_null($request->image)){
            $page->image = $page->saveImage($request->file('image'));
        }

        $page->save();

        return redirect()->route('dashboard.store.pages.index');
    }

    public function destroy(Page $page)
    {
        $page->delete();
        return redirect()->route('dashboard.store.pages.index');
    }
}
