<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ExclusiveProduct;
use App\Models\Option;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;

class MainController extends FrontendController
{
    public function main()
    {
        $sliders = Slider::orderBy('position', 'ASC')->get();

        $sliders->map(function ($slider) {
            $slider->imgPosition = 'Down';

            if($slider->text_type == 'left'){
                $slider->imgPosition = 'right';
            }elseif($slider->text_type == 'right'){
                $slider->imgPosition = 'left';
            }

            return $slider;
        });

        $exclusive = ExclusiveProduct::with('product')->orderBy('main', 'DESC')->get();
        $products = Product::orderBy('id', 'DESC')->take(6)->get();

        $catProd = Category::whereParentId(0)->with(['products' => function($query){
            return $query->take(7);
        }])->get();

        return $this->_view('main', compact('sliders', 'exclusive', 'products', 'catProd'));
    }
}
