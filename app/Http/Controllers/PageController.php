<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends FrontendController
{
    public function show(Page $page)
    {
        return $this->_view('pages.show', compact('page'));
    }
}
