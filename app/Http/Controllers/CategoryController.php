<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CategoryController extends FrontendController
{
    public function index(Product $product)
    {
        $paginate = $product->filter()->paginate(10);
        $categories = Category::with('productsCount')->get();

        return $this->_view('categories.index', compact('categories', 'paginate'));
    }

    public function show(Category $category)
    {
        $paginate = $category->products()->filter()->paginate(10);
        $categories = Category::with('productsCount')->get();

        return $this->_view('categories.show', compact('categories', 'category', 'paginate'));
    }
}
