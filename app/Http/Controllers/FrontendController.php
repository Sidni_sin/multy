<?php

namespace App\Http\Controllers;

use App\Facades\SubDomainService;

class FrontendController extends Controller
{
    public function _view($view = null, $data = [], $mergeData = [])
    {
        $view = 'designs.'.SubDomainService::getDesign().'.'.$view;
        return view($view, $data, $mergeData);
    }
}
