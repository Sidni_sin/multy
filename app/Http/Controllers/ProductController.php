<?php

namespace App\Http\Controllers;

use App\Facades\SubDomainService;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Partners\Factory;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends FrontendController
{
    public function __construct(Factory $factory)
    {
        $this->partner = $factory->create(SubDomainService::getProgram(), SubDomainService::getUserApiKey(), SubDomainService::getUserPrivateKey(), SubDomainService::getUserHash());
    }

    public function show(Product $product)
    {
        $product->load(['comments' => function($query){
            return $query->where('user_id', Auth::id())->orWhere('is_visible', Comment::IS_VISIBLE)->orderBy('id', 'DESC');
        },'comments.user', 'categories']);

        $newPic = with(new Image());
        $newPic->src = $product->picture;
        $product->images->push($newPic);

        $product->specifications = json_decode($product->specifications);

        return $this->_view('products.show', compact('product'));
    }
}
