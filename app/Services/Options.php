<?php

namespace App\Services;

use App\Models\Option;
use Illuminate\Support\Facades\Lang;

class Options
{
    protected $_options = [];
    protected $_baseOptions = null;

    public function __construct(Option $option)
    {
        $tmp = $option->whereAutoload(Option::YES_AUTOLOAD)->get(['name', 'value'])->toArray();

        foreach ($tmp as $o){
            $key = mb_strtolower (str_replace('_', '',$o['name']));
            $this->_options[$key] = $o['value'];
            $this->_baseOptions[$o['name']] = $o['value'];
        }
    }

    public function __call($name, $arguments)
    {
        $keyEvent = substr($name,0,3);
        $bodyEvent = substr($name,3, strlen($name));
        if(method_exists($this, $name)){
            return $this->$name($arguments);
        }elseif ($keyEvent == 'get'){
            if(array_key_exists(mb_strtolower($bodyEvent), $this->_options ?? [])){
                return $this->_options[mb_strtolower($bodyEvent)];
            }
        }

        return null;
    }

    public function getBaseAll()
    {
        return $this->_baseOptions;
    }

    public function getFooterColumns()
    {
        $footer = [];
        $match  = null;
        foreach ($this->_options as $key => $option){
            if(preg_match("/^(footercolumns[0-9]title|footercolumns[0-9]text)/", $key)){
                $footer[] = $option;
            }
        }

        return $footer;
    }


    public function getSocial()
    {
        $social = [];
        $match  = null;
        foreach ($this->_options as $key => $option){
            if(preg_match("/^socialpage+(?)/", $key)){
                $social[str_replace('socialpage', '', $key)] = $option;
            }
        }

        return $social;
    }

    public function getSortedOptions()
    {
        $baseOptions = [];
        $match  = null;

        foreach ($this->_baseOptions as $key => $option){
            if(preg_match("/^meta_main+(?)/", $key)){
                $baseOptions['meta_main'][$key] = ['value' => $option, 'name' => Lang::get('options.'.$key)];
            }
            elseif(preg_match("/^lang+(?)/", $key)){
                $baseOptions['lang'][$key] = ['value' => $option, 'name' => Lang::get('options.'.$key)];
            }
            elseif(preg_match("/^(footer_columns_[0-9]_title|footer_columns_[0-9]_text)/", $key)){
                $baseOptions['footer_columns'][$key] = ['value' => $option, 'name' => Lang::get('options.'.$key)];
            }
            elseif(preg_match("/^button+(?)/", $key)){
                $baseOptions['button'][$key] = ['value' => $option, 'name' => Lang::get('options.'.$key)];
            }
        }

        return $baseOptions;
    }
}