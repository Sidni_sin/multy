<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Page;

class MenuBuild
{
    protected $_categories = null;
    protected $_pages = null;

    public function __construct(Category $category, Page $page)
    {
        $this->_pages      = $page->whereIsVisible(Page::VISIBLE)->get(['title', 'slug']);
        $this->_categories = $category->where('parent_id', Category::PARENT_CATEGORY)->get(['name', 'slug']);
    }

    public function getCategories()
    {
        return $this->_categories;
    }

    public function getPages()
    {
        return $this->_pages;
    }
}