<?php

namespace App\Services;
use App\Models\Store;
use Illuminate\Support\Facades\Route;

class SubDomain
{
    protected $current_store = null;
    protected $_store        = null;

    /**
     * SubDomain constructor.
     * @param Store $store
     * @throws \Exception
     */
    public function __construct(Store $store)
    {
        $this->_store        = $store;
        $this->current_store = $this->_store->where('slug', Route::current()->parameter('store'))->first();
        if(is_null($this->current_store)){
            throw new \Exception('Not store');
        }
    }

    public function getDesign()
    {
        return $this->current_store->design;
    }

    public function getStoreId()
    {
        return $this->current_store->id;
    }

    public function getName()
    {
        return $this->current_store->name;
    }

    public function getSlug()
    {
        return $this->current_store->slug;
    }

    public function getProgram()
    {
        return $this->current_store->affiliate_program;
    }

    public function getUserApiKey()
    {
        return $this->current_store->user_api_key;
    }

    public function getUserHash()
    {
        return $this->current_store->user_hash;
    }

    public function getUserPrivateKey()
    {
        return $this->current_store->user_private_key;
    }
}