<?php

namespace App\Services;

use App\Models\EmbedCode as EmbedCodeModel;

class EmbedCode
{
    protected $embed_code = null;

    public function __construct(EmbedCodeModel $embedCode)
    {
        $this->embed_code = $embedCode->all();
    }

    public function getTop()
    {
        return $this->filter(EmbedCodeModel::TOP_EMBED);
    }

    public function getDown()
    {
        return $this->filter(EmbedCodeModel::DOWN_EMBED);
    }

    private function filter($type)
    {
        return $this->embed_code->filter(function ($embed) use ($type) {
            return ($embed->location == $type);
        })->map(function ($embed){
            return $embed->code;
        });
    }
}