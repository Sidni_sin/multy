<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmbedCodesTable extends Migration
{
    public function up()
    {
        Schema::create('embed_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->text('code');
            $table->smallInteger('location');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('embed_codes');
    }
}
