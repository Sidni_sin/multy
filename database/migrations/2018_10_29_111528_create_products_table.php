<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->unsignedInteger('category_id')->nullable();
            $table->string('slug');
            $table->string('p_id');
            $table->string('p_category_id');
            $table->unsignedInteger('p_store_id')->nullable();
            $table->string('p_store_title')->nullable();
            $table->string('name');
            $table->string('picture');
            $table->unsignedInteger('price');
            $table->unsignedInteger('sale_price');
            $table->string('currency');
            $table->text('description')->nullable();
            $table->string('short_description', 510)->nullable();
            $table->string('specifications', 510)->nullable();
            $table->string('url');
            $table->string('partner_link');
            $table->string('lang');
            $table->unsignedInteger('commission_rate')->nullable();
            $table->smallInteger('is_visible');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
