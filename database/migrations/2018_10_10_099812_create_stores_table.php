<?php

use App\Models\Store;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('manager_id');
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->string('slug')->unique();
            $table->string('name');
            $table->string('user_api_key');
            $table->string('user_hash');
            $table->string('user_private_key');
            $table->string('design')->default(Store::DESIGN_DEFAULT);
            $table->string('affiliate_program')->default('epn');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
