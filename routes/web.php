<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('{store}.domain')->middleware(['web', 'dynamic.subdomain'])->name('frontend.')->group(function () {

    Auth::routes();

    Route::get('/', 'MainController@main')->name('main');

    Route::get('/home', 'HomeController@index');
    Route::resource('products', 'ProductController')->only(['index', 'show']);
    Route::resource('categories', 'CategoryController')->only(['index', 'show']);
    Route::resource('pages', 'PageController')->only(['show']);
    Route::resource('comments', 'CommentController')->only(['store']);

});


Route::domain('aleapi')->group(function () {

    Route::get('/', function () {
        return view('main');
    });

    //dashboard

    Route::middleware(['web', 'auth:manager'])->namespace('Dashboard')->prefix('dashboard')->name('dashboard.')->group(function () {
        Route::resource('stores', 'StoreController');

        //prefix->{store} in middleware->manager.stores
        Route::middleware(['manager.stores', 'dynamic.subdomain'])->prefix('{store}')->name('store.')->group(function () {
            Route::get('/main', 'MainController@index')->name('main');
            Route::resource('sliders', 'SliderController')->except(['show']);
            Route::resource('categories', 'CategoryController')->except(['show']);
            Route::resource('pages', 'PageController')->except(['show']);
            Route::resource('embed-codes', 'EmbedCodeController')->except(['show']);
            Route::resource('products', 'ProductController')->except(['show']);
            Route::get('comments/change/{comment}', 'CommentController@change')->name('comments.change');
            Route::resource('comments', 'CommentController')->only(['index', 'update', 'edit', 'destroy']);
            Route::resource('exclusive-products', 'ExclusiveProductController')->except(['show']);

            Route::get('custom-options', 'OptionController@edit')->name('custom-options.edit');
            Route::put('custom-options', 'OptionController@update')->name('custom-options.update');

            Route::resource('images', 'ImageController')->only(['destroy']);
            Route::post('partner-products', 'PartnerProductController@search')->name('partner.products.search');
//            Route::post('partner-products/{search}', 'PartnerProductController@search')->name('partner.products.search');

        });

    });

    Route::namespace('Dashboard')->name('dashboard.')->group(function () {
        Auth::routes();
    });

});