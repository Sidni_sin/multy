<?php
return [
    'groups' => [
        'dashboard' => [
            'dashboard.*',
        ],
        'frontend' => [
            'frontend.*',
        ]
    ],
];