import ButtonLoader from "../directives/button-loader";
import BootstrapVue from 'bootstrap-vue'

require('./bootstrap');

window.Vue = require('vue');

Vue.component('products-search', require('./components/products/productsSearch.vue'));
Vue.component('products', require('./components/products/products.vue'));
Vue.component('edit-delete-images', require('./components/products/editDeleteImages'));
Vue.component('edit-specifications', require('./components/products/editSpecifications'));
Vue.component('change-visible', require('./components/comments/changeVisible.vue'));
Vue.component('options', require('./components/options/options'));

Vue.use(BootstrapVue);

Vue.directive('buttonLoader', ButtonLoader);

const app = new Vue({
    el: '#app',
    mixins: ['route'],
});