import BootstrapVue from 'bootstrap-vue';
import ButtonLoader from './directives/button-loader';

require('./bootstrap');

window.Vue = require('vue');


Vue.use(BootstrapVue);

Vue.component('login-component', require('./components/LoginComponent.vue'));
Vue.component('register-component', require('./components/RegisterComponent.vue'));

Vue.component('comment-form-product-component', require('./components/product/Comment/CommentFormProductComponent.vue'));
Vue.component('comment-item-product-component', require('./components/product/Comment/CommentItemProductComponent.vue'));
Vue.component('comment-product-component', require('./components/product/Comment/CommentProductComponent.vue'));
Vue.component('product-image', require('./components/product/Images/Images'));

Vue.directive('buttonLoader', ButtonLoader);

const app = new Vue({
    el: '#app'
});
