<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Alieshop - площадка для создание интернет магазина на партнёрской программе от Aliexpress</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta name="description" content="Создание интернет-магазина с продуктами от партнёрской программы Aliexpress. Начините зарабатывать в месте с нами - создайте свой дропшиппинг магазин" />
    <meta name="author" content="Заец Сергей" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{asset('main/assets/plugins/bootstrap/4.1.3/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('main/assets/plugins/font-awesome/5.3/css/all.min.css')}}" rel="stylesheet" />
    <link href="{{asset('main/assets/plugins/animate/animate.min.css')}}" rel="stylesheet" />
    <link href="{{asset('main/assets/css/one-page-parallax/style.min.css')}}" rel="stylesheet" />
    <link href="{{asset('main/assets/css/one-page-parallax/style-responsive.min.css')}}" rel="stylesheet" />
    <link href="{{asset('main/assets/css/one-page-parallax/theme/default.css')}}" id="theme" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('main/assets/plugins/pace/pace.min.js')}}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body data-spy="scroll" data-target="#header" data-offset="51">
<div id="page-container" class="fade">
    <div id="header" class="header navbar navbar-transparent navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand">
                    <span class="brand-logo"></span>
                    <span class="brand-text">
                        <span class="text-theme">Alieshop</span>
                    </span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item"><a class="nav-link" href="#about" data-click="scroll-to-target">О Alieshop</a></li>
                    <li class="nav-item"><a class="nav-link" href="#team" data-click="scroll-to-target">TEAM</a></li>
                    <li class="nav-item"><a class="nav-link" href="#service" data-click="scroll-to-target">SERVICES</a></li>
                    <li class="nav-item"><a class="nav-link" href="#work" data-click="scroll-to-target">WORK</a></li>
                    <li class="nav-item"><a class="nav-link" href="#client" data-click="scroll-to-target">CLIENT</a></li>
                    <li class="nav-item"><a class="nav-link" href="#pricing" data-click="scroll-to-target">PRICING</a></li>
                    <li class="nav-item"><a class="nav-link" href="#contact" data-click="scroll-to-target">CONTACT</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="home" class="content has-bg home">
        <div class="content-bg" style="background-image: url({{asset('main/assets/img/bg/bg-home.jpg')}});"
             data-paroller="true"
             data-paroller-factor="0.5"
             data-paroller-factor-xs="0.25">
        </div>
        <div class="container home-content">
            <h1>Добро пожаловать на Alieshop</h1>
            <h3>Создание интернет магазин</h3>
            <p>
               На площадке вы сможите создать дропшипинг интернет магазин добавить товары и начать зарабатывать
            </p>
            <a href="#" class="btn btn-theme">Видео с примером</a>  <a href="{{route('dashboard.register')}}" class="btn btn-outline">Регистрация</a> <a href="{{route('dashboard.login')}}" class="btn btn-outline">Вход</a>
            <br />

            <br />
            <a href="#">Телеграм канал - Подпишись!</a>
        </div>
    </div>

    <div id="about" class="content" data-scrollview="true">
        <div class="container" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title">О Alieshop</h2>
            <p class="content-desc">
                На Alieshop вы можете создать интернет-магазин в который сможите добавить товары с партёрских программ<br> и быстро получить полноценный дропшиппинг интернет магазин
            </p>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="about">
                        <h3>Услуги</h3>
                        <p>
                            Мы предоставляем вам дропшиппинг сервис,
                        </p>
                        <p>
                            В нём вы легко сможите создать интернет-магазин
                            С товарами из дропшипинг программы от Aliexpress
                            И начать зарабтывать
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h3>Философия</h3>
                    <div class="about-author">
                        <div class="quote bg-silver">
                            <i class="fa fa-quote-left"></i>
                            <h3>Довольный клиет,<br/><span>как источник успеха</span></h3>
                            <i class="fa fa-quote-right"></i>
                        </div>
                        <div class="author">
                            <div class="image">
                                <img src="../assets/img/user/user-1.jpg" alt="Sean Ngu" />
                            </div>
                            <div class="info">
                                Заец Сергей
                                <small>Главный разработчик</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="quote" class="content bg-black-darker has-bg" data-scrollview="true">
        <div class="content-bg" style="background-image: url({{asset('main/assets/img/bg/bg-quote.jpg')}})"
             data-paroller-factor="0.5"
             data-paroller-factor-md="0.01"
             data-paroller-factor-xs="0.01">
        </div>
        <div class="container" data-animation="true" data-animation-type="fadeInLeft">
            <div class="row">
                <div class="col-md-12 quote">
                    <i class="fa fa-quote-left"></i> Страсть ведет к дизайну, дизайн - к производительности, <br />
                    производительность - к <span class="text-theme">успеху !</span>
                    <i class="fa fa-quote-right"></i>
                </div>
            </div>
        </div>
    </div>
    <div id="service" class="content" data-scrollview="true">
        <div class="container">
            <h2 class="content-title">Наши услуги</h2>
            <p class="content-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />
                sed bibendum turpis luctus eget
            </p>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-cog"></i></div>
                        <div class="info">
                            <h4 class="title">Easy to Customize</h4>
                            <p class="desc">Duis in lorem placerat, iaculis nisi vitae, ultrices tortor. Vestibulum molestie ipsum nulla. Maecenas nec hendrerit eros, sit amet maximus leo.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-paint-brush"></i></div>
                        <div class="info">
                            <h4 class="title">Clean & Careful Design</h4>
                            <p class="desc">Etiam nulla turpis, gravida et orci ac, viverra commodo ipsum. Donec nec mauris faucibus, congue nisi sit amet, lobortis arcu.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-file"></i></div>
                        <div class="info">
                            <h4 class="title">Well Documented</h4>
                            <p class="desc">Ut vel laoreet tortor. Donec venenatis ex velit, eget bibendum purus accumsan cursus. Curabitur pulvinar iaculis diam.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-code"></i></div>
                        <div class="info">
                            <h4 class="title">Re-usable Code</h4>
                            <p class="desc">Aenean et elementum dui. Aenean massa enim, suscipit ut molestie quis, pretium sed orci. Ut faucibus egestas mattis.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-shopping-cart"></i></div>
                        <div class="info">
                            <h4 class="title">Online Shop</h4>
                            <p class="desc">Quisque gravida metus in sollicitudin feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-heart"></i></div>
                        <div class="info">
                            <h4 class="title">Free Support</h4>
                            <p class="desc">Integer consectetur, massa id mattis tincidunt, sapien erat malesuada turpis, nec vehicula lacus felis nec libero. Fusce non lorem nisl.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="action-box" class="content has-bg" data-scrollview="true">
        <div class="content-bg" style="background-image: url({{asset('main/assets/img/bg/bg-action.jpg')}})"
             data-paroller-factor="0.5"
             data-paroller-factor-md="0.01"
             data-paroller-factor-xs="0.01">
        </div>
        <div class="container" data-animation="true" data-animation-type="fadeInRight">
            <div class="row action-box">
                <div class="col-md-9 col-sm-9">
                    <div class="icon-large text-theme">
                        <i class="fa fa-binoculars"></i>
                    </div>
                    <h3>CHECK OUT OUR ADMIN THEME!</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus faucibus magna eu lacinia eleifend.
                    </p>
                </div>
                <div class="col-md-3 col-sm-3">
                    <a href="#" class="btn btn-outline btn-block">Live Preview</a>
                </div>
            </div>
        </div>
    </div>

    <div id="work" class="content" data-scrollview="true">
        <div class="container" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title">Our Latest Work</h2>
            <p class="content-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />
                sed bibendum turpis luctus eget
            </p>
        </div>
    </div>

    <div id="client" class="content has-bg bg-green" data-scrollview="true">
        <div class="content-bg" style="background-image: url({{asset('main/assets/img/bg/bg-client.jpg')}})"
             data-paroller-factor="0.5"
             data-paroller-factor-md="0.01"
             data-paroller-factor-xs="0.01">
        </div>
        <div class="container" data-animation="true" data-animation-type="fadeInUp">
            <h2 class="content-title">Our Client Testimonials</h2>
            <div class="carousel testimonials slide" data-ride="carousel" id="testimonials">
                <div class="carousel-inner text-center">
                    <div class="carousel-item active">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce viverra, nulla ut interdum fringilla,<br />
                            urna massa cursus lectus, eget rutrum lectus neque non ex.
                            <i class="fa fa-quote-right"></i>
                        </blockquote>
                        <div class="name"> — <span class="text-theme">Mark Doe</span>, Designer</div>
                    </div>
                    <div class="carousel-item">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            Donec cursus ligula at ante vulputate laoreet. Nulla egestas sit amet lorem non bibendum.<br />
                            Nulla eget risus velit. Pellentesque tincidunt velit vitae tincidunt finibus.
                            <i class="fa fa-quote-right"></i>
                        </blockquote>
                        <div class="name"> — <span class="text-theme">Joe Smith</span>, Developer</div>
                    </div>
                    <div class="carousel-item">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            Sed tincidunt quis est sed ultrices. Sed feugiat auctor ipsum, sit amet accumsan elit vestibulum<br />
                            fringilla. In sollicitudin ac ligula eget vestibulum.
                            <i class="fa fa-quote-right"></i>
                        </blockquote>
                        <div class="name"> — <span class="text-theme">Linda Adams</span>, Programmer</div>
                    </div>
                </div>
                <ol class="carousel-indicators m-b-0">
                    <li data-target="#testimonials" data-slide-to="0" class="active"></li>
                    <li data-target="#testimonials" data-slide-to="1" class=""></li>
                    <li data-target="#testimonials" data-slide-to="2" class=""></li>
                </ol>
            </div>
        </div>
    </div>

    <div id="pricing" class="content" data-scrollview="true">
        <div class="container">
            <h2 class="content-title">Our Price</h2>
            <p class="content-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />
                sed bibendum turpis luctus eget
            </p>
            <ul class="pricing-table pricing-col-4">
                <li data-animation="true" data-animation-type="fadeInUp">
                    <div class="pricing-container">
                        <h3>Starter</h3>
                        <div class="price">
                            <div class="price-figure">
                                <span class="price-number">FREE</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li>1GB Storage</li>
                            <li>2 Clients</li>
                            <li>5 Active Projects</li>
                            <li>5 Colors</li>
                            <li>Free Goodies</li>
                            <li>24/7 Email support</li>
                        </ul>
                        <div class="footer">
                            <a href="#" class="btn btn-inverse btn-block">Buy Now</a>
                        </div>
                    </div>
                </li>
                <li data-animation="true" data-animation-type="fadeInUp">
                    <div class="pricing-container">
                        <h3>Basic</h3>
                        <div class="price">
                            <div class="price-figure">
                                <span class="price-number">$9.99</span>
                                <span class="price-tenure">per month</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li>2GB Storage</li>
                            <li>5 Clients</li>
                            <li>10 Active Projects</li>
                            <li>10 Colors</li>
                            <li>Free Goodies</li>
                            <li>24/7 Email support</li>
                        </ul>
                        <div class="footer">
                            <a href="#" class="btn btn-inverse btn-block">Buy Now</a>
                        </div>
                    </div>
                </li>
                <li class="highlight" data-animation="true" data-animation-type="fadeInUp">
                    <div class="pricing-container">
                        <h3>Premium</h3>
                        <div class="price">
                            <div class="price-figure">
                                <span class="price-number">$19.99</span>
                                <span class="price-tenure">per month</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li>5GB Storage</li>
                            <li>10 Clients</li>
                            <li>20 Active Projects</li>
                            <li>20 Colors</li>
                            <li>Free Goodies</li>
                            <li>24/7 Email support</li>
                        </ul>
                        <div class="footer">
                            <a href="#" class="btn btn-theme btn-block">Buy Now</a>
                        </div>
                    </div>
                </li>
                <li data-animation="true" data-animation-type="fadeInUp">
                    <div class="pricing-container">
                        <h3>Lifetime</h3>
                        <div class="price">
                            <div class="price-figure">
                                <span class="price-number">$999</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li>Unlimited Storage</li>
                            <li>Unlimited Clients</li>
                            <li>Unlimited Projects</li>
                            <li>Unlimited Colors</li>
                            <li>Free Goodies</li>
                            <li>24/7 Email support</li>
                        </ul>
                        <div class="footer">
                            <a href="#" class="btn btn-inverse btn-block">Buy Now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- end container -->
    </div>
    <!-- end #pricing -->

    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Contact Us</h2>
            <p class="content-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />
                sed bibendum turpis luctus eget
            </p>
            <!-- begin row -->
            <div class="row">
                <!-- begin col-6 -->
                <div class="col-md-6" data-animation="true" data-animation-type="fadeInLeft">
                    <h3>If you have a project you would like to discuss, get in touch with us.</h3>
                    <p>
                        Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus.
                    </p>
                    <p>
                        <strong>SeanTheme Studio, Inc</strong><br />
                        795 Folsom Ave, Suite 600<br />
                        San Francisco, CA 94107<br />
                        P: (123) 456-7890<br />
                    </p>
                    <p>
                        <span class="phone">+380 66 22 43 712</span><br />
                        <a href="mailto:hello@emailaddress.com">support@alieshop.ru</a>
                    </p>
                </div>

            </div>
        </div>
    </div>
    <!-- end #contact -->

    <!-- begin #footer -->
    <div id="footer" class="footer">
        <div class="container">
            <div class="footer-brand">
                <div class="footer-brand-logo"></div>
                Color Admin
            </div>
            <p>
                &copy; Copyright Color Admin 2018 <br />
                An admin & front end theme with serious impact. Created by <a href="#">SeanTheme</a>
            </p>
            <p class="social-list">
                <a href="#"><i class="fab fa-facebook-f fa-fw"></i></a>
                <a href="#"><i class="fab fa-instagram fa-fw"></i></a>
                <a href="#"><i class="fab fa-twitter fa-fw"></i></a>
                <a href="#"><i class="fab fa-google-plus-g fa-fw"></i></a>
                <a href="#"><i class="fab fa-dribbble fa-fw"></i></a>
            </p>
        </div>
    </div>
    <!-- end #footer -->
</div>
<!-- end #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('main/assets/plugins/jquery/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('main/assets/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js')}}"></script>
<!--[if lt IE 9]>
    <script src="{{asset('main/assets/crossbrowserjs/html5shiv.js')}}"></script>
    <script src="{{asset('main/assets/crossbrowserjs/respond.min.js')}}"></script>
    <script src="{{asset('main/assets/crossbrowserjs/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('main/assets/plugins/js-cookie/js.cookie.js')}}"></script>
<script src="{{asset('main/assets/plugins/scrollMonitor/scrollMonitor.js')}}"></script>
<script src="{{asset('main/assets/plugins/paroller/jquery.paroller.min.js')}}"></script>
<script src="{{asset('main/assets/js/one-page-parallax/apps.min.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>
