@extends('designs.'.SubDomain::getDesign().'.layouts.default')

@section('tags')

    <title>{{$category->name}}</title>

    <meta name="description" content="{{$category->short_description}}" />

    <meta property="og:title" content="{{$category->name}}" />
    <meta property="og:description" content="{{$category->description}}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="shop" />
    <meta property="og:url" content="{{route('frontend.categories.show', $category->slug)}}" />

    <meta name="twitter:title" content="{{$category->name}}" />
    <meta name="twitter:description" content="{{$category->description}}">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@<?= OptionsFacade::getMetaMainSite()?>" />


@endsection

@section('content')
    <div id="search-results" class="section-container bg-silver">
        <div class="container">
            <div class="search-container">
                <div class="search-content">
                    <div class="search-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Категория "{{($category->productsCount) ? $category->productsCount->count : '0'}})</h4>
                            </div>
                            <div class="col-md-6 text-right">
                                <ul class="sort-list">
                                    <li class="text"><i class="fa fa-filter"></i> Сортировать:</li>
                                    {{--<li class="active"><a href="#">Popular</a></li>--}}
                                    <li>
                                        <a href="{{request()->fullUrlWithQuery(['sale' => request()->has('sale')?!request('sale'):1, 'price_by' => null])}}">По скидке</a>
                                        @if(request()->has('sale'))
                                            <span>{!! request('sale')?'&uarr;':'&darr;' !!}</span>
                                        @endif
                                    </li>
                                    <li>
                                        <a href="{{request()->fullUrlWithQuery(['price_by' => request()->has('price_by')?!request('price_by'):1, 'sale' => null])}}">По цене</a>
                                        @if(request()->has('price_by'))
                                            <span>{!! request('price_by')?'&uarr;':'&darr;' !!}</span>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="search-item-container">
                        @foreach(with(new Illuminate\Support\Collection($paginate->items()))->chunk(3) as $chunk)
                            <div class="item-row">
                                @foreach($chunk as $product)
                                    <div class="item item-thumbnail">
                                        <a href="{{route('frontend.products.show', $product->slug)}}" class="item-image">
                                            <img src="{{asset($product->picture)}}" alt="" />
                                        </a>
                                        <div class="item-info">
                                            <h4 class="item-title">
                                                <a href="{{route('frontend.products.show', $product->slug)}}">{{$product->name}}</a>
                                            </h4>
                                            <p class="item-desc">{{$product->short_description}}</p>
                                            @if($product->price != $product->sale_price)
                                                <div class="item-discount-price">
                                                    <span>{{$product->price}} <small>{{$product->currency}}</small></span>
                                                </div>
                                            @endif
                                            <div class="item-price">
                                                <span>{{($product->sale_price != $product->price)?$product->sale_price:$product->price}}<small> {{$product->currency}}</small></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">
                        {{ $paginate->appends(request()->except('page'))->links() }}
                    </div>
                </div>
                <div class="search-sidebar">
                    <h4 class="title">Фильтровать по</h4>
                    <form action="{{route('frontend.categories.show', $category->slug)}}" method="get" name="filter_form">
                        <div class="form-group">
                            <label class="control-label">Поиск</label>
                            <a class="pull-right" href="{{route('frontend.categories.show', $category->slug)}}">Сбросить</a>
                            <input type="text" class="form-control input-sm" name="s" value="{{request('s')}}" placeholder="Enter Keywords" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Цена</label>
                            <div class="row row-space-0">
                                <div class="col-md-5">
                                    <input type="number" class="form-control input-sm" name="price_from" value="{{request('price_from')??1}}" placeholder="От" />
                                </div>
                                <div class="col-md-2 text-center p-t-5 f-s-12 text-muted">до</div>
                                <div class="col-md-5">
                                    <input type="number" class="form-control input-sm" name="price_to" value="{{request('price_to')??50000}}" placeholder="До" />
                                </div>
                            </div>
                        </div>
                        <div class="m-b-30">
                            <button type="submit" class="btn btn-sm btn-inverse"><i class="fa fa-search"></i> Поиск</button>
                        </div>
                    </form>
                    <h4 class="title m-b-0">Категории</h4>
                    <ul class="search-category-list">
                        <li>
                            <a href="{{route('frontend.categories.index')}}">
                                Все категории
                                <span class="pull-right">></span>
                            </a>
                        </li>
                        @foreach($categories as $cat)
                            @if(!is_null($cat->productsCount))
                                <li><a href="{{route('frontend.categories.show', $cat->slug)}}">{{$cat->name}}<span class="pull-right">{{$cat->productsCount->count}}</span></a></li>
                            @endIf
                        @endForeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection