@extends('designs.'.SubDomain::getDesign().'.layouts.default')

@section('tags')

    <title>{{$page->title}}</title>

    <meta name="description" content="{{$page->description}}" />
    <meta name="keywords" content="{{$page->keywords}}">

    <meta property="og:title" content="{{$page->title}}" />
    <meta property="og:description" content="{{$page->description}}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="shop" />
    <meta property="og:url" content="{{route('frontend.pages.show', $page->slug)}}" />

    <meta name="twitter:title" content="{{$page->title}}" />
    <meta name="twitter:description" content="{{$page->description}}">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@<?= OptionsFacade::getMetaMainSite()?>" />

@endsection

@section('content')
    <div id="about-us-cover" class="has-bg section-container">
        <div class="cover-bg">
            <img src="{{asset($page->image)}}" alt="" />
        </div>
        <div class="container">
            <ul class="breadcrumb m-b-10 f-s-12">
                <li class="breadcrumb-item"><a href="{{route('frontend.main')}}">Главная</a></li>
                <li class="breadcrumb-item active">{{$page->title}}</li>
            </ul>
            <div class="about-us text-center">
                <h1>{{$page->title}}</h1>
                <p>
                    {{$page->description}}
                </p>
            </div>
        </div>
    </div>

    <div id="about-us-content" class="section-container bg-white">
        <div class="container">
            {!! $page->text !!}
        </div>
    </div>

@endsection