<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('tags')

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/plugins/bootstrap/4.1.3/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/plugins/font-awesome/5.3/css/all.min.css')}}" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/plugins/animate/animate.min.css')}}" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/css/e-commerce/style.min.css')}}" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/css/e-commerce/style-responsive.min.css')}}" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/css/e-commerce/theme/default.css')}}" id="theme" rel="stylesheet" />
    <link href="{{asset('frontend/e-commerce/css/e-commerce/main.css')}}" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('frontend/e-commerce/plugins/pace/pace.min.js')}}"></script>

    @routes('frontend')

    <!-- ================== END BASE JS ================== -->
    <script>
        @foreach(EmbedCodeFacade::getTop() as $code)

        @endforeach
    </script>
</head>
<body>
<div id="app">
    <div id="page-container" class="fade">

        <div id="top-nav" class="top-nav">
            <div class="container">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @foreach(OptionsFacade::getSocial() as $key => $link)
                            @if(!empty($link))
                                <li><a href="{{$link}}" target="_blank"><i class="fab fa-{{$key}} f-s-14"></i></a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div id="header" class="header">
            <div class="container">
                <div class="header-container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="header-logo">
                            <a href="{{route('frontend.main')}}">
                                {{--<span class="brand">--}}
                                    {{--<img src="../assets/img/user/user-1.jpg" alt="">--}}
                                {{--</span>--}}
                                <span>{{SubDomain::getName()}}</span>
                            </a>
                        </div>
                    </div>
                    <div class="header-nav">
                        <div class=" collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav">
                                <li class="active"><a href="{{route('frontend.main')}}">{{OptionsFacade::getLangMain()}}</a></li>

                                <li class="dropdown dropdown-hover">
                                    <a href="#" data-toggle="dropdown">
                                        {{OptionsFacade::getLangCategory()}}
                                        <b class="caret"></b>
                                        <span class="arrow top"></span>
                                    </a>
                                    <div class="dropdown-menu">
                                        @foreach(MenuBuildFacade::getCategories() as $c)
                                            <a class="dropdown-item" href="{{route('frontend.categories.show', $c->slug)}}">{{$c->name}}</a>
                                        @endforeach
                                    </div>
                                </li>
                                <li class="dropdown dropdown-hover">
                                    <a href="#" data-toggle="dropdown">
                                        {{OptionsFacade::getLangPage()}}
                                        <b class="caret"></b>
                                        <span class="arrow top"></span>
                                    </a>
                                    <div class="dropdown-menu">
                                        @foreach(MenuBuildFacade::getPages() as $p)
                                            <a class="dropdown-item" href="{{route('frontend.pages.show', $p->slug)}}">{{$p->title}}</a>
                                        @endforeach
                                    </div>
                                </li>
                                <li class="dropdown dropdown-hover">
                                    <a href="#" data-toggle="dropdown">
                                        <i class="fa fa-search search-btn"></i>
                                        <span class="arrow top"></span>
                                    </a>
                                    <div class="dropdown-menu p-15">
                                        <form action="{{route('frontend.categories.index')}}" method="get" name="filter_form">
                                            <div class="input-group">
                                                <input type="text" placeholder="Search" name="s" class="form-control bg-silver-lighter" />
                                                <div class="input-group-append">
                                                    <button class="btn btn-inverse" type="submit"><i class="fa fa-search"></i></button>
                                                </div>
                                                <input style="display: none;" name="price_to" value="50000"/>
                                                <input style="display: none;" name="price_from" value="1"/>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-nav">
                        <ul class="nav pull-right">
                            <li class="divider"></li>
                            @guest
                                <li>
                                    <a>
                                        <b-btn v-b-modal.modal-login>{{OptionsFacade::getLangLogin()}}</b-btn>
                                        <b-btn v-b-modal.modal-signup>{{OptionsFacade::getLangRegister()}}</b-btn>
                                    </a>
                                </li>
                            @else

                                <li class="dropdown dropdown-hover">
                                    <a href="#" data-toggle="dropdown">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAYFBMVEVVYIDn7O3///++xM9ueJOttMKorr64vsp8hZ3X2uGiqLno7e3w8fRXYoFcZ4XT2d6Sma6CiqFkbouMlKlzfJb4+frL0Njr7PBncY3g4uiborTS1d7g5efv8vPCyNF2f5mQoSweAAAFXUlEQVR4nMWb25qrIAyFsWOt2mI9n3qY93/LrXVqPQBZsfLtdTUXU/ILIUAI4rBFl+DHi/3crdNIyjT3szg8BZdNTQnuD4qbl0RCqToJy8IuwC30pdr4W9L3AlsATVibjb+VtgwGFKDyXMz6IPeIegQG0MREzyvGIsO6AQG4JVzrgxIEgQYI/G3mQQQKoOJ3/hyh+gqgOGqmPK7o+AVAwPJ8nX6brQDeHuY7ydAQH/UA1RfOt5Sv9wQtwPPr0Z8qvXEB9ur+UR4PIN7bfjch1Y6gBCg2hj6zfCWBCqDKbdjv5qNqgVIAXHaZ/Sq5CoI1AGo/cv2s9U4nr818F5wy7no6rgCKX6Sh8Hp3ZrpfQwQ8X/nBEqCgw0/alo5SZZuSP04oANL/66fa+qAnuWuLzQBk/MnuJvvdUGRUC54J4Eb8ODqZzfc6UQ751ANUxBimV9q+41yJVqJKB0A5oDQO/0dPYhPl6wBC8+9EiNl3HKohTw3QUOCofceh5nKgBKAikGb2q1QSTbmFAuBI/CjH7TsOtZwd1wAVNXuOHADqaz4zYQQgQyARgea6U63FS4CA+oXLse841MokgwUA2QEZD4CMyP4cgOwAPAgMokKBELcZAL0J9HgA9K46mQLQHcCbBAiAaCYA5IjZAIg/ABfgCL4/gKxGACps2AEY1qQXALKdtADgvgEAF7QC8HLDHqD9XwDhHwC9mbYEUA8A0AhYAeijoUBTAVYAwhcAloqxAuD3AAWWCLQCIIsOgNq/2QTonECQm3GrAF4HgKVj0oAHcMUyBkkHgN1C/PDsO84P1Gx0EBfoH1l78kFYoukisDDU8gGgAN+ZP0H/BxzLl8Ia/hHYJGC7AOoEnsByotYAYgFsB20C+AJbCawB5AKbLdYAXAHtRuwB1AJbC60BpP8bIAKHwFogkgJbi2I+ABbhpMCS8zUfAJvftUCy853AHOVHT6zdHAxEImIk6XpdwWKHRKD3UxGUJx6/H/Nt0a0E8AUdZ0sAjmvfqoBvKBl+iIWAlzwBOovgpGoZ144n0cD/C+fJroyah0CAByPBSFViu8GXZIFuy3vdQAB0BvQfBR9MeoHzAFsEBmX40ayTxEIB5+bZww+nvaAuQFJuo0r8eN4L6YKGU3khL32CgjFrgVWZVfrwSlBAaco3MTkRSlbd0/EFgIci4JCKrwK9Gkaa7k9EOOTVvvyl6VgVK9K4IvAGQLScVO1bpoB85/SlGC5N4GT1qLMe4MqzX4/Zck7w3BHg+Lmw4ASP3QCiy+fKhuOGuwFMrmxYbrgbwPTSCswX7wrgMy8udwco51e3WKamk9TbZwG8C4reAFT5xChjIGIANAsAeCIkBgAH31+uru/pAoY/GVMV8H44uqwAwJsb894cPhF86mgmRSzIVKSOqODmJlcVsSCDUJPHM6gf5aTUeVrIRK1JsgXqSEpgaZ1We89qyczb0wzMEJwohFlNIVzM5jNSJGaEVFvMdjjcdF7sMhOFBgRZHvQAGjdImVdmRoRFuf+ypFNxUoxCVg0TgdAezACrVUnG28xrELKlvXVZ7zweoa4PIvySZb3dBnFyuOG4PoKgKK1WlHaPfcB1fRJBVVyuKm4fiuu3uL4ZQVlery7v7zwxaXaz3ymUugcGugcO+33+oDJfllSbAQ6FYe+5SQ+NIe0jl30JztqHPoZ3Ro/97Os+3wywWyfoP58A2KkTDJ9PAhyK7+0Tz2DJ94ZfdoL58xGArxBI8+Cb040IgHn41e2DPSGMrs8H4M5J1Dzr4XOBdsPjwXgAznv6TTOcOcb5AAOE1ie5xrcBDBQdxuNxfvXH+dz/XWww3usfQ+1UAQiHFoIAAAAASUVORK5CYII=" class="user-img" alt="" />
                                    </a>
                                    <div class="dropdown-menu p-15">
                                        <a href="{{ route('frontend.logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Выход
                                        </a>
                                        <form id="logout-form" action="{{ route('frontend.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        <div id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <?php
                        $optionsFooter = OptionsFacade::getFooterColumns();

                        $count = (int)count($optionsFooter);
                        if($count == 4)
                            $row = 6;
                        elseif($count == 6){
                            $row = 4;
                        }else{
                            $row = 3;
                        }
                    ?>
                    @for($i = 0; $i < $count; $i+=2)
                        <div class="col-md-{{$row}}">
                            <h4 class="footer-header">{{$optionsFooter[$i]}}</h4>
                            <p>
                                {{$optionsFooter[$i+1]}}
                            </p>
                        </div>
                    @endfor
                </div>
            </div>
        </div>

        <div id="modal-vue" class="mt-15 modal-vue">
            @guest
                <login-component></login-component>
                <register-component></register-component>
            @endguest
        </div>
    </div>
</div>

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('frontend/e-commerce/plugins/jquery/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('frontend/e-commerce/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('frontend/e-commerce/plugins/js-cookie/js.cookie.js')}}"></script>
<script src="{{asset('frontend/e-commerce/plugins/paroller/jquery.paroller.min.js')}}"></script>
<script src="{{asset('frontend/e-commerce/js/e-commerce/apps.min.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script>
    @foreach(EmbedCodeFacade::getDown() as $code)
        <?php echo $code; ?>
    @endforeach
</script>
</body>
</html>
