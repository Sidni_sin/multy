@extends('designs.'.SubDomain::getDesign().'.layouts.default')

@section('tags')
    <?php
    $title = OptionsFacade::getMetaMainTitle();
    $keywords = OptionsFacade::getMetaMainKeywords();
    $description = OptionsFacade::getMetaMainDescription();
    ?>
    <title>{{$title}}</title>

    <meta name="description" content="{{$description}}" />
    <meta name="keywords" content="{{$keywords}}" />

    <meta property="og:title" content="{{$title}}" />
    <meta property="og:description" content="{{$description}}" />
    <meta property="og:locale" content="{{OptionsFacade::getMetaMainLocale()}}" />
    <meta property="og:type" content="shop" />
    <meta property="og:url" content="{{route('frontend.main')}}" />

    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$description}}">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@<?= OptionsFacade::getMetaMainSite()?>" />

    <link rel="image_src" href="{{asset(OptionsFacade::getMetaMainImage())}}" />
@endsection

@section('content')
    @if(!$sliders->isEmpty())
        <div id="slider" class="section-container p-0 bg-black-darker">
            <div id="main-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($sliders as $i => $slider)
                        <div class="carousel-item @if($i == 0) active @endif" data-paroller="true" data-paroller-factor="0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url({{asset($slider->background_src)}}) center -100px / cover no-repeat;">

                            @if(!is_null($slider->src))
                                <div class="container">
                                    <img src="{{asset($slider->src)}}" class="product-img {{$slider->imgPosition}} bottom fadeInRight animated" alt="" />
                                </div>
                            @endif

                            <div class="carousel-caption carousel-caption-{{$slider->text_type}}">
                                <div class="container">
                                    <h3 class="title m-b-5 fadeIn{{ucfirst($slider->text_type)}}Big animated">{{$slider->title}}</h3>
                                    <p class="m-b-15 fadeIn{{ucfirst($slider->text_type)}}Big animated">{{$slider->title}}</p>
                                    @if(!is_null($slider->price))
                                        <div class="price m-b-30 fadeIn{{ucfirst($slider->text_type)}}Big animated"><span>{{$slider->price}} .грн</span></div>
                                    @endif
                                    <a href="{{$slider->url_button}}" class="btn btn-outline btn-lg fadeIn{{ucfirst($slider->text_type)}}Big animated">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                <a class="carousel-control-prev" href="#main-carousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control-next" href="#main-carousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    @endif

    @if(!$exclusive->isEmpty())
        <div id="promotions" class="section-container bg-white">
            <div class="container">
                <h4 class="section-title clearfix">
                    Эксклюзивное предложение
                    <small></small>
                </h4>
                <div class="row row-space-10">
                    <div class="col-md-6">
                        <?php $mainExclusive = $exclusive->shift(); ?>
                        <div class="promotion promotion-lg bg-black-darker">
                            <div class="promotion-image text-right promotion-image-overflow-bottom">
                                <img src="{{$mainExclusive->product->picture}}" alt="" />
                            </div>
                            <div class="promotion-caption promotion-caption-inverse">
                                <h4 class="promotion-title">{{substr($mainExclusive->product->name, 0, 40)}}</h4>
                                <div class="promotion-price">{{$mainExclusive->product->price}} {{$mainExclusive->product->currency}}</div>
                                <p class="promotion-desc">{{$mainExclusive->product->name}}</p>
                                <a href="{{route('frontend.products.show', $mainExclusive->product->slug)}}" class="promotion-btn">View More</a>
                            </div>
                        </div>
                    </div>
                    @foreach($exclusive->chunk(2) as $exclu)
                        <div class="col-md-3 col-sm-6">
                            @foreach($exclu as $exc)
                                <div class="promotion bg-{{$exc->background_color}}">
                                    <div class="promotion-image text-{{($exc->text_layout == 'right') ? 'left' : ($exc->text_layout == 'left') ? 'right' : 'center'}} promotion-image-overflow-bottom promotion-image-overflow-top">
                                        <img src="{{asset($exc->product->picture)}}" alt="" />
                                    </div>
                                    <div class="promotion-caption text-{{$exc->text_layout}} {{$exc->background_color == 'silver' ? '': 'promotion-caption-inverse'}}">
                                        <h4 class="promotion-title">{{substr($exc->product->name, 0, 15)}}</h4>
                                        <div class="promotion-price">{{$exc->product->price}} {{$exc->product->currency}}</div>
                                        <p class="promotion-desc">{{substr($exc->product->name, 0, 60)}}</p>
                                        <a href="{{route('frontend.products.show', $exc->product->slug)}}" class="promotion-btn">View More</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div id="trending-items" class="section-container bg-silver">
        <div class="container">
            @if(!$products->isEmpty())
                <h4 class="section-title clearfix">
                    Последние продукты
                    <small></small>
                </h4>
                <div class="row row-space-10">
                    @foreach ($products as $product)
                        <div class="col-md-2 col-sm-4">
                            <div class="item item-thumbnail">
                                <a href="{{route('frontend.products.show', $product->slug)}}" class="item-image">
                                    <img src="{{asset($product->picture)}}" alt="" />
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="{{route('frontend.products.show', $product->slug)}}">{{substr($product->name, 0, 15)}}</a>
                                    </h4>
                                    <p class="item-desc">{{substr($product->name, 0, 30)}}</p>
                                    @if($product->price != $product->sale_price)
                                        <div class="item-discount-price">
                                            <span>{{$product->price}} <small>{{$product->currency}}</small></span>
                                        </div>
                                    @endif
                                    <div class="item-price">
                                        <span>{{($product->sale_price != $product->price)?$product->sale_price:$product->price}}<small> {{$product->currency}}</small></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <div id="mobile-list" class="section-container bg-silver p-t-0">
        <div class="container">
            <h4 class="section-title clearfix">
                <a href="{{route('frontend.categories.index')}}" class="pull-right">{{OptionsFacade::getButtonShowAll()}}</a>
                Категории
            </h4>
            @if(!is_null($catProd))
                <div class="category-container">
                    <div class="category-sidebar">
                        <ul class="category-list">
                            <li class="list-header">Главные категории</li>
                            @foreach($catProd as $key => $cat)
                                <li><a class="@if($key == 0) category-list-active-a @endif " href="{{route('frontend.categories.show', $cat->slug)}}">{{$cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <?php
                        $prodFirstCat = null;
                        foreach ($catProd as $c){
                            if(!$c->products->isEmpty()){
                                $prodFirstCat = $c->products;
                                break;
                            }
                        }
                        $first = is_null($prodFirstCat) ? null : $prodFirstCat->first();
                    ?>
                    @if(!is_null($first))
                        <div class="category-detail">
                            <a href="{{route('frontend.products.show', $first->slug)}}" class="category-item full">
                                <div class="item">
                                    <div class="item-cover">
                                        <img src="{{$first->picture}}" alt="" />
                                    </div>
                                    <div class="item-info bottom">
                                        <h4 class="item-title">{{substr($first->name, 0, 20)}}</h4>
                                        <p class="item-desc">{{substr($first->name, 0, 60)}}</p>
                                        @if($first->price != $first->sale_price)
                                            <div class="item-discount-price">
                                                <span>{{$first->price}} <small>{{$first->currency}}</small></span>
                                            </div>
                                        @endif
                                        <div class="item-price">
                                            <span>{{($first->sale_price != $first->price)?$first->sale_price:$first->price}}<small> {{$first->currency}}</small></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            @if(!is_null($prodFirstCat) && !$prodFirstCat->isEmpty())
                                <div class="category-item list">
                                    @foreach($prodFirstCat->chunk(3) as $prods)
                                        <div class="item-row">
                                            @foreach($prods as $p)
                                                <div class="item item-thumbnail">
                                                    <a href="{{route('frontend.products.show', $p->slug)}}" class="item-image">
                                                        <img src="{{asset($p->picture)}}" alt="" />
                                                    </a>
                                                    <div class="item-info">
                                                        <h4 class="item-title">
                                                            <a href="{{route('frontend.products.show', $p->slug)}}">{{substr($p->name, 0, 15)}}</a>
                                                        </h4>
                                                        <p class="item-desc">{{substr($p->name, 0, 60)}}</p>
                                                        @if($p->price != $p->sale_price)Услуги
                                                            <div class="item-discount-price">
                                                                <span>{{$p->price}} <small>{{$p->currency}}</small></span>
                                                            </div>
                                                        @endif
                                                        <div class="item-price">
                                                            <span>{{($p->sale_price != $p->price)?$p->sale_price:$p->price}}<small> {{$p->currency}}</small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>

@endsection
