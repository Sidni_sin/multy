@extends('designs.'.SubDomain::getDesign().'.layouts.default')

@section('tags')

    <title>{{$product->name }} | {{config('app.name')}}</title>

    <meta name="description" content="{{$product->short_description}}" />

    <meta property="og:title" content="{{$product->name}}" />
    <meta property="og:description" content="{{$product->short_description}}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="shop" />
    <meta property="og:url" content="{{route('frontend.products.show', $product->slug)}}" />
    <meta property="og:image" content="{{asset($product->picture ?? '')}}" />

    <meta name="twitter:title" content="{{$product->name}}" />
    <meta name="twitter:description" content="{{$product->short_description}}">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@<?= OptionsFacade::getMetaMainSite()?>" />

    <link rel="image_src" href="{{asset($product->picture ?? '')}}" />

@endsection

@section('content')
    <div id="product" class="section-container p-t-20">
        <div class="container">
            {!! $product->breadCrumbHtml('breadcrumb m-b-10 f-s-12', true) !!}

            <div class="product">
                <div class="product-detail">
                    <product-image :images='@json($product->images)'></product-image>
                    <div class="product-info">
                        <div class="product-info-header">
                            <h1 class="product-title">
                                {{$product->name}}
                            </h1>
                            {!! $product->breadCrumbHtml('product-category') !!}
                        </div>

                        @if(!is_null($product->specifications))
                            <ul class="product-info-list">
                                @foreach($product->specifications as $info)
                                    <li><i class="fa fa-circle"></i>{{$info->value}}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="product-social">
                            <ul>
                                @foreach(OptionsFacade::getSocial() as $key => $link)
                                    @if(!is_null($link))
                                        <li>
                                            <a href="{{$link}}" target="_blank" class="facebook" data-toggle="tooltip" data-trigger="hover" data-title="{{$key}}" data-placement="top">
                                                <i class="fab fa-{{$key}}"></i>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>

                        <div class="product-purchase-container">
                            @if(($product->price != $product->sale_price))
                                <div class="product-discount">
                                    <span class="discount">{{$product->price}} <small> {{$product->currency}}.</small></span>
                                </div>
                            @endif
                            <div class="product-price">
                                <div class="price">{{($product->sale_price == $product->price)?$product->price:$product->sale_price}}<small> {{$product->currency}}</small></div>
                            </div>
                            <div>
                                <a href="{{asset($product->partner_link)}}" class="btn btn-inverse btn-lg">Купить</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-detail " style="background-color: #edf0f3">
                    <div class="p-20">
                        {{$product->short_description}}
                    </div>
                </div>
                <div class="product-tab">
                    <ul id="product-tab" class="nav nav-tabs">
                        <li><a href="#product-desc" class="active show" data-toggle="tab">Описание продукта</a></li>
                        @if(!is_null($product->specifications))
                            <li><a href="#product-info" data-toggle="tab">Характеристики</a></li>
                        @endif
                        <li><a href="#product-reviews" data-toggle="tab">Отзывы ({{count($product->comments)}})</a></li>
                    </ul>
                    <div id="product-tab-content" class="tab-content">
                        <div class="tab-pane fade active in" id="product-desc">
                        {!! $product->description !!}
                        </div>
                        @if(!is_null($product->specifications))
                            <div class="tab-pane fade" id="product-info">
                                <h2>{{$product->name}}</h2>
                                <div class="table-responsive">
                                    <table class="table table-product table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($product->specifications as $info)
                                            <tr>
                                                <td class="field">{{$info->key}}</td>
                                                <td>
                                                    {{$info->value}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    @endif
                        <div class="tab-pane fade" id="product-reviews">
                            <comment-product-component :id='@json($product->id)' :auth-check="@json(Auth::check())" :comments='@json($product->comments)'></comment-product-component>
                        </div>
                    </div>

                </div>
            </div>

            @if(!is_null($product->categories))
                @foreach($product->categories as $category)
                    <h4 class="m-b-15 m-t-30">Из категории "{{$category->name}}"</h4>
                    <div class="row row-space-10">
                        @foreach($category->products as $product)
                            <div class="col-md-2 col-sm-4">
                                <div class="item item-thumbnail">
                                    <a href="{{route('frontend.products.show', $product->slug)}}" class="item-image">
                                        <img src="{{asset($product->picture)}}" alt="">
                                    </a>
                                    <div class="item-info">
                                        <h4 class="item-title">
                                            <a href="{{route('frontend.products.show', $product->slug)}}">{{$product->name}}</a>
                                        </h4>
                                        <p class="item-desc">{{$product->short_description}}</p>
                                        @if($product->price != $product->sale_price)
                                            <div class="item-discount-price">
                                                <span>{{$product->price}} <small> {{$product->currency}}.</small></span>
                                            </div>
                                        @endif
                                        <div class="item-price">
                                            <span>{{($product->sale_price != $product->price)?$product->sale_price:$product->price}}<small> {{$product->currency}}.</small></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection