@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Слайдер</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Создание слайдера</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>

                                <form action="{{route('dashboard.store.sliders.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Заголовок <span class="color-red">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" name="title" placeholder="Заголовок" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" value="{{ old('title') }}">
                                            @if ($errors->has('title'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('title') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Текст <span class="color-red">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" name="text" placeholder="Текст" class="form-control {{ $errors->has('text') ? ' parsley-error' : '' }}" value="{{ old('text') }}">

                                            @if ($errors->has('text'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('text') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Цена</label>
                                        <div class="col-md-6">
                                            <input type="text" name="price" placeholder="Цена" class="form-control {{ $errors->has('price') ? ' parsley-error' : '' }}" value="{{ old('price') }}">

                                            @if ($errors->has('price'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('price') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Ссылка на кнопке <span class="color-red">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" name="url_button" placeholder="Ссылка на кнопке" class="form-control {{ $errors->has('url_button') ? ' parsley-error' : '' }}" value="{{ old('url_button') }}">

                                            @if ($errors->has('url_button'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('url_button') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Картинка</label>
                                        <div class="col-lg-6">
                                            <input type="file" class="p-t-5" name="image">

                                            @if ($errors->has('image'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('image') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Картинка фона <span class="color-red">*</span></label>
                                        <div class="col-lg-6">
                                            <input type="file" class="p-t-5" name="background_image">

                                            @if ($errors->has('background_image'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('background_image') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Расположение текста</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="text_type" id="text_type">
                                                <option value="center" selected="selected">По центру</option>
                                                <option value="left">С лева</option>
                                                <option value="right">С права</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
