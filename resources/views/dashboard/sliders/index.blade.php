@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.sliders.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Создать слайдер</a></li>
        </ol>
        <h1 class="page-header">Слайдер</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Слайдер главной страницы</h4>
            </div>
            <div class="panel-body">
                @if(!is_null($sliders))
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Поз.</th>
                                <th width="1%" data-orderable="false">Картинка</th>
                                <th class="text-nowrap">Заголовок</th>
                                <th class="text-nowrap">Текст</th>
                                <th class="text-nowrap">Ссылка на кнопке</th>
                                <th class="text-nowrap">Тип</th>
                                <th class="text-nowrap">Действие</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($sliders as $key => $slider)
                            <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                <td width="1%" class="f-s-600 text-inverse">{{$slider->position}}</td>
                                <td width="1%" class="with-img"><img src="{{asset($slider->src)}}" class="img-rounded height-30" /></td>
                                <td>{{$slider->title}}</td>
                                <td>{{$slider->text}}</td>
                                <td>{{$slider->url_button}}</td>
                                <td>{{$slider->text_type}}</td>
                                <td>
                                    <a href="{{route('dashboard.store.sliders.edit', $slider->id)}}"  class="btn btn-outline-primary btn-sm">Edit</a>
                                    <form method="post" action="{{route('dashboard.store.sliders.destroy', $slider->id)}}" class="btn p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
