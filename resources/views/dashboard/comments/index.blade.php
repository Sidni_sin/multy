@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <h1 class="page-header">Комментарии</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Пользователю видны его и все комментарии которые вы разрешили к показу</h4>
            </div>
            <div class="panel-body">
                @if(!$comments->isEmpty())
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Заголовок</th>
                                <th>Текст</th>
                                <th>Оценка</th>
                                <th class="text-nowrap">Действие</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($comments as $key => $comment)
                                <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                    <td>{{$comment->title}}</td>
                                    <td>{{$comment->text}}</td>
                                    <td>{{$comment->assessment}}</td>
                                    <td width="20%">
                                        <a href="{{route('dashboard.store.comments.edit', $comment->id)}}"  class="btn btn-outline-primary btn-sm">Редактировать</a>
                                        <form method="post" action="{{route('dashboard.store.comments.destroy', $comment->id)}}" class="btn p-0">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
                                        </form>
                                        <change-visible :is_visible.number="@json($comment->is_visible)" :id.number="@json($comment->id)"></change-visible>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection