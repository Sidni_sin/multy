@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Комментарии</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Пользователю видны его и все комментарии которые вы разрешили к показу</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <form action="{{route('dashboard.store.comments.update', $comment->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Зголовок</label>
                                        <div class="col-md-6">
                                            <input type="text" name="title" placeholder="Заголовок" class="form-control" value="{{ old('title') ?? $comment->title }}">

                                            @if ($errors->has('title'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Текст</label>
                                        <div class="col-md-6">
                                            <input type="text" name="text" placeholder="Текст" class="form-control" value="{{ old('text') ?? $comment->text }}">

                                            @if ($errors->has('text'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('text') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Оценка</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="assessment" id="text_type">
                                                <option value="1" @if ($comment->assessment == 1) selected="selected" @endif >1</option>
                                                <option value="2" @if ($comment->assessment == 2) selected="selected" @endif >2</option>
                                                <option value="3" @if ($comment->assessment == 3) selected="selected" @endif >3</option>
                                                <option value="4" @if ($comment->assessment == 4) selected="selected" @endif >4</option>
                                                <option value="5" @if ($comment->assessment == 5) selected="selected" @endif >5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Показать</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="is_visible">
                                                <option value="1" @if ($comment->is_visible == '1') selected="selected" @endif >Да</option>
                                                <option value="0" @if ($comment->is_visible == '0') selected="selected" @endif >Нет</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection