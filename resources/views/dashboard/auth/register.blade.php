@extends('dashboard.layouts.app')

@section('content')
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>

    <div id="page-container" class="fade">
        <div class="register register-with-news-feed">
            <div class="news-feed">
                <div class="news-image" style="background-image: url(../assets/img/login-bg/login-bg-9.jpg)"></div>
                <div class="news-caption">
                    <h4 class="caption-title"><b>Магазин</b> </h4>
                    <p>
                        Создайте свой магазин на партнёрской программе Aliexpress
                    </p>
                </div>
            </div>
            <div class="right-content">
                <h1 class="register-header">
                    Регистрация
                    <small>Создайте свой магазин</small>
                </h1>
                <div class="register-content">
                    <form action="{{ route('dashboard.register') }}" method="POST" class="margin-bottom-0">
                        @csrf

                        <label class="control-label">Имя <span class="text-danger">*</span></label>
                        <div class="row row-space-10">
                            <div class="col-md-12 m-b-15">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Имя" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <label class="control-label">Email <span class="text-danger">*</span></label>
                        <div class="row m-b-15">
                            <div class="col-md-12">
                                <input id="email" type="email" placeholder="Email адрес" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <label class="control-label">Пароль <span class="text-danger">*</span></label>
                        <div class="row m-b-15">
                            <div class="col-md-12">
                                <input id="password" placeholder="Пароль" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <label class="control-label">Повторите пароль <span class="text-danger">*</span></label>
                        <div class="row m-b-15">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Пароль" required>
                            </div>
                        </div>
                        <div class="register-buttons">
                            <button type="submit" class="btn btn-primary btn-block btn-lg">Зарегистрироваться</button>
                        </div>
                    </form>

                    <div class="m-t-20 text-inverse pull-right">
                        <a href="{{ route('dashboard.login') }}" class="text-success">Вход</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
