@extends('dashboard.layouts.app')

@section('content')
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <!-- begin login-cover -->
    <div class="login-cover">
        <div class="login-cover-image" style="background-image: url({{asset('assets/img/login-bg/login-bg-17.jpg')}})" data-id="login-cover-image"></div>
        <div class="login-cover-bg"></div>
    </div>
    <!-- end login-cover -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> <b>Востановить</b>
                    <small>Востановление пароля</small>
                </div>
                <div class="icon">
                    <i class="fa fa-lock"></i>
                </div>
            </div>
            <!-- end brand -->
            <!-- begin login-content -->
            <div class="login-content">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('dashboard.password.email') }}" method="POST" class="margin-bottom-0">
                    @csrf

                    <div class="form-group m-b-20">
                        <input id="email" type="email" placeholder="Email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Востановить</button>
                    </div>

                    <div class="m-t-20 text-inverse">
                        <a href="{{ route('dashboard.register') }}" class="text-success">Регистрация</a>
                        <a href="{{ route('dashboard.login') }}" class="text-success pull-right">Вход</a>
                    </div>
                </form>
            </div>
            <!-- end login-content -->
        </div>

    </div>
@endsection
