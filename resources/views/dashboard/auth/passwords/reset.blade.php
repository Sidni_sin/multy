@extends('dashboard.layouts.app')

@section('content')
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <!-- begin login-cover -->
    <div class="login-cover">
        <div class="login-cover-image" style="background-image: url({{asset('assets/img/login-bg/login-bg-17.jpg')}})" data-id="login-cover-image"></div>
        <div class="login-cover-bg"></div>
    </div>
    <!-- end login-cover -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> <b>Изменить пароль</b>
                    <small>Изменение пароль</small>
                </div>
                <div class="icon">
                    <i class="fa fa-lock"></i>
                </div>
            </div>
            <!-- end brand -->
            <!-- begin login-content -->
            <div class="login-content">
                <form action="{{ route('dashboard.password.update') }}" method="POST" class="margin-bottom-0">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group m-b-20">
                        <input id="email" type="email" placeholder="Email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group m-b-20">
                        <input id="password" type="password" placeholder="Новый пароль" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group m-b-20">
                        <input id="password-confirm" type="password" placeholder="Повторить пароль" class="form-control form-control-lg" name="password_confirmation">
                    </div>

                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Востановить</button>
                    </div>

                    <div class="m-t-20 text-inverse">
                        <a href="{{ route('dashboard.register') }}" class="text-success">Регистрация</a>
                        <a href="{{ route('dashboard.login') }}" class="text-success pull-right">Вход</a>
                    </div>
                </form>
            </div>
            <!-- end login-content -->
        </div>

    </div>
@endsection
