@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.pages.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Создать страницу</a></li>
        </ol>
        <h1 class="page-header">Страницы</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Страницы</h4>
            </div>
            <div class="panel-body">
                @if(!is_null($pages))
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Поз.</th>
                                <th width="1%" data-orderable="false">Картинка</th>
                                <th class="text-nowrap">slug</th>
                                <th class="text-nowrap">Заголовок</th>
                                <th class="text-nowrap">Описание</th>
                                <th class="text-nowrap">Keywords</th>
                                <th class="text-nowrap">Показывать</th>
                                <th class="text-nowrap">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $key => $page)
                            <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                <td width="1%" class="f-s-600 text-inverse">{{$page->id}}</td>
                                <td width="4%" class="with-img"><img src="{{asset($page->image)}}" class="img-rounded height-150" /></td>
                                <td>{{$page->slug}}</td>
                                <td>{{$page->title}}</td>
                                <td>{{$page->description}}</td>
                                <td>{{$page->keywords}}</td>
                                <td>{{$page->is_visible ? 'Да' : 'Нет'}}</td>
                                <td>
                                    <a href="{{route('dashboard.store.pages.edit', $page->slug)}}"  class="btn btn-outline-primary btn-sm">Edit</a>
                                    <form method="post" action="{{route('dashboard.store.pages.destroy', $page->slug)}}" class="btn p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection