@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Страницы</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Страницы</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>

                                <form action="{{route('dashboard.store.pages.update', $page->slug)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row m-b-12">
                                        <div class="col-md-12">
                                            <textarea name="text" id="editor">{{ old('text') ?? $page->text }}</textarea>
                                            @if ($errors->has('text'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('text') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Slug</label>
                                        <div class="col-md-6">
                                            <input type="text" name="slug" placeholder="Slug" class="form-control {{ $errors->has('slug') ? ' parsley-error' : '' }}" value="{{ old('slug') ?? $page->slug }}">
                                            @if ($errors->has('slug'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('slug') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Заголовок</label>
                                        <div class="col-md-6">
                                            <input type="text" name="title" placeholder="Заголовок" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" value="{{ old('title') ?? $page->title }}">
                                            @if ($errors->has('title'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('title') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Описание</label>
                                        <div class="col-md-6">
                                            <input type="text" name="description" placeholder="Описание" class="form-control {{ $errors->has('description') ? ' parsley-error' : '' }}" value="{{ old('description') ?? $page->description }}">

                                            @if ($errors->has('description'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('description') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Keywords</label>
                                        <div class="col-md-6">
                                            <input type="text" name="keywords" placeholder="Keywords" class="form-control {{ $errors->has('keywords') ? ' parsley-error' : '' }}" value="{{ old('keywords') ?? $page->keywords }}">

                                            @if ($errors->has('keywords'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('keywords') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Картинка</label>
                                        <div class="col-lg-3">
                                            <input type="file" class="p-t-5" name="image">

                                            @if ($errors->has('image'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('image') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                        <div class="col-lg-3">
                                            <span>Сохранённое фото</span>
                                            <img src="{{$page->image}}" class="height-150" alt="">
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Показать</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="is_visible">
                                                <option value="1" @if( $page->is_visible == 1) selected="selected" @endif >Да</option>
                                                <option value="0" @if( $page->is_visible == 0) selected="selected" @endif>Нет</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('end-body-scripts')

    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/ckeditor/samples/js/sample.js') }}"></script>

    <script>
        var editor = CKEDITOR.replace( 'editor',{
            filebrowserBrowseUrl : '/elfinder/ckeditor',
            height : '500',
        });
    </script>
@endpush