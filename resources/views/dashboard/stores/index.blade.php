@extends('dashboard.layouts.dashboard')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.stores.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Создать магазин</a></li>
        </ol>
        <h1 class="page-header">Ваши магазины </h1>
        <div class="row">
            <div class="col-lg-2">
                <h5>Данные:</h5>
                <ul class="p-l-25 m-b-15">
                    <li>Магазинов: {{$user->quantity_store}}</li>
                    <li>Лимити: {{$user->limit_store}}</li>
                </ul>
            </div>
            <div class="col-lg-10">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Магазины</h4>
                    </div>
                    <div class="panel-body">
                        @if(!is_null($stores))
                            <table id="data-table-combine" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="1%"></th>
                                    <th width="1%" data-orderable="false">Лого</th>
                                    <th class="text-nowrap">Название</th>
                                    <th class="text-nowrap">Slug</th>
                                    <th class="text-nowrap">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($stores as $store)
                                        <tr class="odd gradeX">
                                            <td width="1%" class="f-s-600 text-inverse">1</td>
                                            <td width="1%" class="with-img"><img src="../assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>
                                            <td>{{$store->name}}</td>
                                            <td>{{$store->slug}}</td>
                                            <td class="text-center">
                                                <a href="{{route('dashboard.stores.edit', $store->slug)}}" id="add-regular" class="btn btn-sm btn-inverse">Конфигурация</a>
                                                <a href="{{route('dashboard.store.main', ['store' => $store->slug])}}" id="add-regular" class="btn btn-sm btn-inverse">Управление контентом</a>
                                                <a href="http://{{$store->slug}}.ale1.ru/" id="add-regular" class="btn btn-sm btn-inverse">Сайт магазина</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
