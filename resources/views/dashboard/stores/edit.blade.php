@extends('dashboard.layouts.dashboard')

@section('content')
    <div id="content" class="content">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Создание</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Данные вашего магазина</legend>

                                <form action="{{route('dashboard.stores.update', $store->slug)}}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Название магазина</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" placeholder="Наименование" class="form-control" value="{{ old('name') ?? $store->name }}">

                                            @if ($errors->has('name'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Slug</label>
                                        <div class="col-md-6">
                                            <input type="text" name="slug" placeholder="Slug" class="form-control" value="{{ old('slug') ?? $store->slug }}">

                                            @if ($errors->has('slug'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('slug') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">user_api_key</label>
                                        <div class="col-md-6">
                                            <input type="text" name="user_api_key" placeholder="" class="form-control" value="{{ old('user_api_key') ?? $store->user_api_key }}">

                                            @if ($errors->has('user_api_key'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('user_api_key') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">user_private_key</label>
                                        <div class="col-md-6">
                                            <input type="text" name="user_private_key" placeholder="" class="form-control" value="{{ old('user_private_key') ?? $store->user_private_key }}">

                                            @if ($errors->has('user_private_key'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('user_private_key') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">user_hash</label>
                                        <div class="col-md-6">
                                            <input type="text" name="user_hash" placeholder="" class="form-control" value="{{ old('user_hash') ?? $store->user_hash }}">

                                            @if ($errors->has('user_hash'))
                                                <span role="alert">
                                                    <strong>{{ $errors->first('user_hash') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
