@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.exclusive-products.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Создать эксклюзивный продукт</a></li>
        </ol>
        <h1 class="page-header">Эксклюзивные продукты</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Эксклюзивные продукты</h4>
            </div>
            <div class="panel-body">
                @if(!$exclusives->isEmpty())
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Поз.</th>
                                <th class="with-img width-100"></th>
                                <th class="text-nowrap">Имя\Заголовок</th>
                                <th class="text-nowrap">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($exclusives as $key => $exclusive)
                            <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                <td width="1%" class="f-s-600 text-inverse">{{$key+1}}</td>
                                <td class="width-100"><img src="{{$exclusive->product->picture}}" class="img-rounded height-100" alt=""></td>
                                <td>{{$exclusive->product->name}}</td>
                                <td>
                                    <a href="{{route('dashboard.store.exclusive-products.edit', $exclusive->id)}}"  class="btn btn-outline-primary btn-sm">Edit</a>
                                    <form method="post" action="{{route('dashboard.store.exclusive-products.destroy', $exclusive->id)}}" class="btn p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection