@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Эксклюзтвные продукты</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Эксклюзтвные продукты</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>

                                <form class="form-valide" action="{{route('dashboard.store.exclusive-products.store')}}" method="post">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Выберите продукт<span class="text-danger">*</span></label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="product_id" id="text_type">
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Это главный продукт ?</label>
                                        <div class="col-lg-6">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="main" name="main">
                                                <label class="form-check-label" for="main">Главный ?</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Цвет фона</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="background_color" id="text_type">

                                                <option value="silver" selected="selected">Серый</option>
                                                <option value="black" >Тёмный</option>
                                                <option value="blue" >Голубой</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Расположение текста</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="text_layout" id="text_type">

                                                <option value="center" selected="selected">по центру</option>
                                                <option value="right" >с права</option>
                                                <option value="left" >с лева</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="submit" class="btn btn-primary">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('end-body-scripts')

    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/ckeditor/samples/js/sample.js') }}"></script>

    <script>
        var editor = CKEDITOR.replace( 'editor',{
            filebrowserBrowseUrl : '/elfinder/ckeditor',
        });
    </script>
@endpush