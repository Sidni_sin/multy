@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Эксклюзивные продукты</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Эксклюзивные продукты</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>

                                <form class="form-valide" action="{{route('dashboard.store.exclusive-products.update', $exclusiveProduct->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Check product in exclusive<span class="text-danger">*</span></label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="product_id" id="text_type">
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}" @if($product->id == $exclusiveProduct->product_id) selected="selected" @endif>{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Цвет фона</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="background_color" id="text_type">

                                                <option value="silver" @if($exclusiveProduct->background_color == 'silver') selected="selected" @endif>Серый</option>
                                                <option value="black" @if($exclusiveProduct->background_color == 'black') selected="selected" @endif>Тёмный</option>
                                                <option value="blue" @if($exclusiveProduct->background_color == 'blue') selected="selected" @endif>Голубой</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">Расположение текста</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="text_layout" id="text_type">

                                                <option value="center" @if($exclusiveProduct->text_layout == 'center') selected="selected" @endif>по центру</option>
                                                <option value="right" @if($exclusiveProduct->text_layout == 'right') selected="selected" @endif>с права</option>
                                                <option value="left" @if($exclusiveProduct->text_layout == 'left') selected="selected" @endif>с лева</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="text_type">This is main product in exclusive ?</label>
                                        <div class="col-lg-6">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="main" @if($exclusiveProduct->main) checked @endif name="main">
                                                <label class="form-check-label" for="main">It is Main ?</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('end-body-scripts')

    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/ckeditor/samples/js/sample.js') }}"></script>

    <script>
        var editor = CKEDITOR.replace( 'editor',{
            filebrowserBrowseUrl : '/elfinder/ckeditor',
            height : '500',
        });
    </script>
@endpush