@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.embed-codes.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Встроить код</a></li>
        </ol>
        <h1 class="page-header">Встраиваемый код</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Код</h4>
            </div>
            <div class="panel-body">
                @if(!$embedCode->isEmpty())
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Поз.</th>
                                <th>Код</th>
                                <th class="text-nowrap">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($embedCode as $key => $code)
                                <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">{{$key+1}}</td>
                                    <td><code>{{$code->code}}</code></td>
                                    <td width="20%">
                                        <a href="{{route('dashboard.store.embed-codes.edit', $code->id)}}"  class="btn btn-outline-primary btn-sm">Редактировать</a>
                                        <form method="post" action="{{route('dashboard.store.embed-codes.destroy', $code->id)}}" class="btn p-0">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection