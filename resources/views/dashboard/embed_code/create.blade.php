<?php
    use App\Models\EmbedCode;
?>
@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Страницы</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Код для каждой страницы</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>
                                <p>
                                    Вставте код которй должен выполняться на каждой странице, например <b>счётчик посещений</b> или любой другой js/html код
                                </p>
                                <form action="{{route('dashboard.store.embed-codes.store')}}" method="POST">
                                    @csrf

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Код</label>
                                        <div class="col-md-6">
                                            <textarea name="code"  class="height-150 form-control {{ $errors->has('code') ? ' parsley-error' : '' }}">{{ old('code') }}</textarea>

                                            @if ($errors->has('code'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('code') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Вставить</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="location">
                                                <option value="<?= EmbedCode::DOWN_EMBED ?>" >В низ страницы</option>
                                                <option value="<?= EmbedCode::TOP_EMBED ?>" >В верх страницы</option>
                                                <option value="<?= EmbedCode::NOT_EMBED ?>" >Не вставлять</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection