@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Категории</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Создание категории</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>

                                <form action="{{route('dashboard.store.categories.update', $category->slug)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Название</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" placeholder="Название" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" value="{{ old('name') ?? $category->name }}">

                                            @if ($errors->has('name'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('name') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Описание</label>
                                        <div class="col-md-6">
                                            <input type="text" name="description" placeholder="Описание" class="form-control {{ $errors->has('description') ? ' parsley-error' : '' }}" value="{{ old('description') ?? $category->description }}">

                                            @if ($errors->has('description'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('description') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">keywords</label>
                                        <div class="col-md-6">
                                            <input type="text" name="keywords" placeholder="слово1, слово2" class="form-control {{ $errors->has('keywords') ? ' parsley-error' : '' }}" value="{{ old('keywords') ?? $category->keywords }}">

                                            @if ($errors->has('keywords'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('keywords') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Позиция</label>
                                        <div class="col-md-6">
                                            <input type="text" name="position" placeholder="1" class="form-control {{ $errors->has('position') ? ' parsley-error' : '' }}" value="{{ old('position') ?? $category->position }}">

                                            @if ($errors->has('position'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('position') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    @if (!empty($tree))
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 text-md-right col-form-label">Родительская категория</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="parent_id">
                                                    <option value="0">Главная</option>
                                                    @foreach($tree as $item)
                                                        <option value="{{$item['id']}}" @if($item['id'] == $category->parent_id) selected="selected" @endif >{{$item['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('parent_id'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('parent_id') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Показать</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="is_visible">
                                                <option value="1" @if ($category->is_visible == '1') selected="selected" @endif >Да</option>
                                                <option value="0" @if ($category->is_visible == '0') selected="selected" @endif >Нет</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
