@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.categories.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Создать категорию</a></li>
        </ol>
        <h1 class="page-header">Категории</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Категории главной страницы</h4>
            </div>
            <div class="panel-body">
                @if(!is_null($tree))
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%"></th>
                                <th class="text-nowrap">Slug</th>
                                <th class="text-nowrap">Имя</th>
                                <th class="text-nowrap">Описание</th>
                                <th class="text-nowrap">keywords</th>
                                <th class="text-nowrap">Позиция</th>
                                <th class="text-nowrap">Показать</th>
                                <th class="text-nowrap">Действие</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tree as $key => $category)
                                <tr class="@if ($category['level'] != 1) odd @else even @endif gradeX">
                                    <td >{{$category['id']}}</td>
                                    <td>{{$category['slug']}}</td>
                                    <td>{{$category['name']}}</td>
                                    <td>{{$category['description']}}</td>
                                    <td>{{$category['keywords']}}</td>
                                    <td>{{$category['position']}}</td>
                                    <td>{{$category['is_visible']}}</td>
                                    <td>
                                        <a href="{{route('dashboard.store.categories.edit', $category['slug'])}}"  class="btn btn-outline-primary btn-sm">Edit</a>
                                        <form method="post" action="{{route('dashboard.store.categories.destroy', $category['slug'])}}" class="btn p-0">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
