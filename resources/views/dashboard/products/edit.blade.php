@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">

        <h1 class="page-header">Товары</h1>
        <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                </div>
                <h4 class="panel-title">Редактирование товара</h4>
            </div>
            <div class="panel-body">
                <div class="sw-container" >
                    <div class="step-content">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <legend class="f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>
                                <form action="{{route('dashboard.store.products.update', $product->slug)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row m-b-12">
                                        <div class="col-md-12">
                                            <textarea name="description" id="editor">{{ old('description') ?? $product->description }}</textarea>
                                            @if ($errors->has('description'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('description') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Имя\Заголовок</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" placeholder="Имя\Заголовок" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" value="{{ old('name') ?? $product->name }}">

                                            @if ($errors->has('name'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('name') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Slug</label>
                                        <div class="col-md-6">
                                            <input type="text" name="slug" placeholder="Slug" class="form-control {{ $errors->has('slug') ? ' parsley-error' : '' }}" value="{{ old('slug') ?? $product->slug }}">

                                            @if ($errors->has('slug'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('slug') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Краткое описание</label>
                                        <div class="col-md-6">
                                            <input type="text" name="short_description" placeholder="Краткое описание" class="form-control {{ $errors->has('short_description') ? ' parsley-error' : '' }}" value="{{ old('short_description') ?? $product->short_description }}">

                                            @if ($errors->has('short_description'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('short_description') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Цена</label>
                                        <div class="col-md-6">
                                            <input type="text" name="price" placeholder="Цена" class="form-control {{ $errors->has('price') ? ' parsley-error' : '' }}" value="{{ old('price') ?? $product->price }}">

                                            @if ($errors->has('price'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('price') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Цена с скидкой</label>
                                        <div class="col-md-6">
                                            <input type="text" name="sale_price" placeholder="Цена с скидкой" class="form-control {{ $errors->has('sale_price') ? ' parsley-error' : '' }}" value="{{ old('sale_price') ?? $product->sale_price }}">

                                            @if ($errors->has('sale_price'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('sale_price') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Валюта</label>
                                        <div class="col-md-6">
                                            <input type="text" name="currency" placeholder="Валюта" class="form-control {{ $errors->has('currency') ? ' parsley-error' : '' }}" value="{{ old('currency') ?? $product->currency }}">

                                            @if ($errors->has('currency'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('currency') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Язык</label>
                                        <div class="col-md-6">
                                            <input type="text" name="lang" placeholder="Валюта" class="form-control {{ $errors->has('lang') ? ' parsley-error' : '' }}" value="{{ old('lang') ?? $product->lang }}">

                                            @if ($errors->has('lang'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('lang') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div>
                                        <edit-specifications specifications="{{old('specifications') ?? $product->specifications}}"></edit-specifications>
                                        @if ($errors->has('specifications'))
                                            <ul class="parsley-errors-list filled">
                                                <li class="parsley-pattern">{{ $errors->first('specifications') }}</li>
                                            </ul>
                                        @endif
                                    </div>

                                    @if(!empty($tree))
                                        <div class="form-group row m-b-10">
                                            <label class="col-md-3 text-md-right col-form-label">Зажмите Ctrl и выберите категории в которых должен отображаться продукт</label>
                                            <div class="col-md-6">
                                                <select class="form-control height-200" name="categories[]" multiple >
                                                    @foreach($tree as $category)
                                                        <option value="{{$category['id']}}" @if(in_array($category['id'], $selectIds)) selected="selected" @endif >{{$category['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Главное фото продукта</label>
                                        <div class="col-lg-3">
                                            <input type="file" class="p-t-5" name="picture">

                                            @if ($errors->has('picture'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('picture') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                        <div class="col-lg-3">
                                            <span>Сохранённое фото</span>
                                            <img src="{{$product->picture}}" class="height-100" alt="">
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Фотографии продукта</label>
                                        <div class="col-lg-3">
                                            <input type="file" class="p-t-5" name="pictures[]" multiple>

                                            @if ($errors->has('pictures'))
                                                <ul class="parsley-errors-list filled">
                                                    <li class="parsley-pattern">{{ $errors->first('pictures') }}</li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 text-md-right col-form-label">Сохранённые фото</label>
                                        <div class="col-lg-9">
                                            <edit-delete-images images='@json($product->images)'></edit-delete-images>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('end-body-scripts')

    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/ckeditor/samples/js/sample.js') }}"></script>

    <script>
        var editor = CKEDITOR.replace( 'editor', {
            filebrowserBrowseUrl : '/elfinder/ckeditor',
            height : '500',
        });
    </script>
@endpush