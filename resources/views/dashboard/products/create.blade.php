@extends('dashboard.layouts.dashboard-store')

@section('content')
    <products :categories='@json($treeCats)'></products>
@endsection