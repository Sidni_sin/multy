@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.store.products.create')}}" id="add-regular" class="btn btn-sm btn-inverse">Добавить продукт</a></li>
        </ol>
        <h1 class="page-header">Продукты</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Продукты</h4>
            </div>
            <div class="panel-body">
                @if(!$products->isEmpty())
                    <table id="data-table-default" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Поз.</th>
                                <th class="with-img width-100"></th>
                                <th>Титле</th>
                                <th class="text-nowrap">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key => $product)
                                <tr class="@if ($key % 2) odd @else even @endif gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">{{$key+1}}</td>
                                    <td class="width-100"><img src="{{$product->picture}}" class="img-rounded height-100" alt=""></td>
                                    <td>{{$product->name}}</td>
                                    <td width="20%">
                                        <a href="{{route('dashboard.store.products.edit', $product->slug)}}"  class="btn btn-outline-primary btn-sm">Редактировать</a>
                                        <form method="post" action="{{route('dashboard.store.products.destroy', $product->slug)}}" class="btn p-0">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
                                        </form>
                                        <a href="{{$product->url}}" class="btn btn-sm btn-inverse">На оригинал продукта</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection