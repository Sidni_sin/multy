<div id="sidebar" class="sidebar">
    <div data-scrollbar="true" data-height="100%">
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:;" data-toggle="nav-profile">
                    <div class="cover with-shadow"></div>
                    <div class="image image-icon bg-black text-grey-darker">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="info">
                        <b class="caret pull-right"></b>
                        Sean Ngu
                    </div>
                </a>
            </li>
            <li>
                <ul class="nav nav-profile">
                    <li><a href="javascript:;"><i class="fa fa-cog"></i> Настройки</a></li>
                    <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Тикеты</a></li>
                    <li><a href="javascript:;"><i class="fa fa-question-circle"></i>Помощь</a></li>
                    <li><a href="{{route('dashboard.stores.index')}}"><i class="fa fa-question-circle"></i>Магазины</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Главная страница</span>
                </a>
                <ul class="sub-menu">
                    <li class="active"><a href="{{route('dashboard.store.sliders.index')}}">Слайдер</a></li>
                    <li><a href="{{route('dashboard.store.exclusive-products.index')}}">Эксклюзивные</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('dashboard.store.categories.index')}}">
                    <i class="fas fa-cubes"></i>
                    <span>Категории</span>
                </a>
            </li>
            <li>
                <a href="{{route('dashboard.store.products.index')}}">
                    <i class="fas fa-cube"></i>
                    <span>Продукты</span>
                </a>
            </li>
            <li>
                <a href="{{route('dashboard.store.comments.index')}}">
                    <i class="fas fa-comments"></i>
                    <span>Комментарии</span>
                </a>
            </li>
            <li>
                <a href="{{route('dashboard.store.pages.index')}}">
                    <i class="fas fa-globe"></i>
                    <span>Cтраницы</span>
                </a>
            </li>
            <li>
                <a href="{{route('dashboard.store.embed-codes.index')}}">
                    <i class="fa fa-code"></i>
                    <span>Встраиваемый код</span>
                </a>
            </li>

            <li>
                <a href="{{route('dashboard.store.custom-options.edit')}}">
                    <i class="fa fa-cog"></i>
                    <span>Опции магазина</span>
                </a>
            </li>
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
        </ul>
    </div>
</div>
<div class="sidebar-bg"></div>