@extends('dashboard.layouts.dashboard-store')

@section('content')
    <div id="content" class="content">
        <h1 class="page-header">Опции сайта</h1>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Опции сайта</h4>
            </div>
            <div class="panel-body">
                @if(!is_null($options))

                    <options :options='@json($options)'></options>

                @endif
            </div>
        </div>
    </div>
@endsection